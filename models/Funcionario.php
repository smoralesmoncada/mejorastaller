<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "funcionario".
 *
 * @property int $rut_fun
 * @property string $nombres_fun
 * @property string $apellido_p_fun
 * @property string $apellido_m_fun
 * @property string $cargo_fun
 *
<<<<<<< HEAD
 * @property Transgresion[] $transgresions
=======
 * @property Denuncia[] $denuncias
>>>>>>> origin/Tobal
 */
class Funcionario extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'funcionario';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['rut_fun'], 'required'],
            [['rut_fun'], 'default', 'value' => null],
            [['rut_fun'], 'integer'],
            [['nombres_fun', 'apellido_p_fun', 'apellido_m_fun', 'cargo_fun'], 'string', 'max' => 30],
            [['rut_fun'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'rut_fun' => 'Rut funcionario',
            'nombres_fun' => 'Nombres',
            'apellido_p_fun' => 'Apellido paterno',
            'apellido_m_fun' => 'Apellido materno',
            'cargo_fun' => 'Cargo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransgresions()
    {
        return $this->hasMany(Transgresion::className(), ['rut_fun' => 'rut_fun']);
    }


    public function getNombreF() {

    $nombreF = (! empty ( $this->nombres_fun )) ? $this->nombres_fun : '';

    $nombreF .= (! empty ( $this->apellido_p_fun )) ? ((! empty ( $nombreF )) ? " " . $this->apellido_p_fun : $this->apellido_p_fun) : '';

    $nombreF .= (! empty ( $this->apellido_m_fun )) ? ((! empty ( $nombreF )) ? " " . $this->apellido_m_fun : $this->apellido_m_fun) : '';

    return $nombreF;

    }
}

