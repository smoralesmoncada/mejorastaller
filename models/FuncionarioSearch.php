<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Funcionario;

/**
 * FuncionarioSearch represents the model behind the search form of `app\models\Funcionario`.
 */
class FuncionarioSearch extends Funcionario
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['rut_fun'], 'integer'],
            [['nombres_fun', 'apellido_p_fun', 'apellido_m_fun', 'cargo_fun'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Funcionario::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'rut_fun' => $this->rut_fun,
        ]);

        $query->andFilterWhere(['ilike', 'nombres_fun', $this->nombres_fun])
            ->andFilterWhere(['ilike', 'apellido_p_fun', $this->apellido_p_fun])
            ->andFilterWhere(['ilike', 'apellido_m_fun', $this->apellido_m_fun])
            ->andFilterWhere(['ilike', 'cargo_fun', $this->cargo_fun]);

        return $dataProvider;
    }
}
