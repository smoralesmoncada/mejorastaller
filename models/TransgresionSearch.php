<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Transgresion;

/**
 * TransgresionSearch represents the model behind the search form of `app\models\Transgresion`.
 */
class TransgresionSearch extends Transgresion
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_transgresion', 'rut_alumno', 'rut_fun', 'id_asocia'], 'integer'],
            [['desc_transgresion'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Transgresion::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_transgresion' => $this->id_transgresion,
            'rut_alumno' => $this->rut_alumno,
            'rut_fun' => $this->rut_fun,
            'id_asocia' => $this->id_asocia,
        ]);

        $query->andFilterWhere(['ilike', 'desc_transgresion', $this->desc_transgresion]);

        return $dataProvider;
    }
}
