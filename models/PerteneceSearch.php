<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Pertenece;

/**
 * PerteneceSearch represents the model behind the search form of `app\models\Pertenece`.
 */
class PerteneceSearch extends Pertenece
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_situacion', 'rut_alumno', 'id_asocia', 'id_cargo'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Pertenece::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_situacion' => $this->id_situacion,
            'rut_alumno' => $this->rut_alumno,
            'id_asocia' => $this->id_asocia,
            'id_cargo' => $this->id_cargo,
        ]);

        return $dataProvider;
    }
}
