<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "asociacion".
 *
 * @property int $id_asocia
 * @property int $id_plan
 * @property int $id_estado_asocia
 * @property string $nombre_asocia
 * @property string $fecha_inicio_asocia
 * @property string $fecha_termino_asocia
 *
 * @property EstadoAsocia $estadoAsocia
 * @property Planificacion $plan
 * @property Denuncia[] $denuncias
 * @property Pertenece[] $perteneces
 * @property RendicionFondo[] $rendicionFondos
 * @property SolicitudCrearAsociacion[] $solicitudCrearAsociacions
 * @property SolicitudFondos[] $solicitudFondos
 */
class Asociacion extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'asociacion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            //[['id_plan'], 'required'],
            [['id_plan', 'id_estado_asocia'], 'default', 'value' => null],
            [['id_plan', 'id_estado_asocia'], 'integer'],
            [['fecha_inicio_asocia', 'fecha_termino_asocia'], 'safe'],
            [['nombre_asocia','fecha_termino_asocia','fecha_inicio_asocia'],'required'],
            [['nombre_asocia'], 'string', 'max' => 50],
            ['fecha_termino_asocia','mayorque'],
            [['nombre_asocia'], 'unique'],
            [['id_estado_asocia'], 'exist', 'skipOnError' => true, 'targetClass' => EstadoAsocia::className(), 'targetAttribute' => ['id_estado_asocia' => 'id_estado_asocia']],
            [['id_plan'], 'exist', 'skipOnError' => true, 'targetClass' => Planificacion::className(), 'targetAttribute' => ['id_plan' => 'id_plan']],
        ];
    }

    public function mayorque($attribute,$params)

    {

       if (date("Y-m-d",strtotime($this->fecha_termino_asocia)) <= date("Y-m-d",strtotime($this->fecha_inicio_asocia)))

          $this->addError($attribute, 'Error, la fecha de término debe ser posterior a la de inicio');

    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_asocia' => 'Código',
            'id_plan' => 'Planificación',
            'id_estado_asocia' => 'Estado',
            'nombre_asocia' => 'Nombre asociación',
            'fecha_inicio_asocia' => 'Fecha inicio',
            'fecha_termino_asocia' => 'Fecha Término',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEstadoAsocia()
    {
        return $this->hasOne(EstadoAsocia::className(), ['id_estado_asocia' => 'id_estado_asocia']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlan()
    {
        return $this->hasOne(Planificacion::className(), ['id_plan' => 'id_plan']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDenuncias()
    {
        return $this->hasMany(Denuncia::className(), ['id_asocia' => 'id_asocia']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerteneces()
    {
        return $this->hasMany(Pertenece::className(), ['id_asocia' => 'id_asocia']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRendicionFondos()
    {
        return $this->hasMany(RendicionFondo::className(), ['id_asocia' => 'id_asocia']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSolicitudCrearAsociacions()
    {
        return $this->hasMany(SolicitudCrearAsociacion::className(), ['id_asocia' => 'id_asocia']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSolicitudFondos()
    {
        return $this->hasMany(SolicitudFondos::className(), ['id_asocia' => 'id_asocia']);
    }

    public function getAsociacionesActivas()
    {
        $activas = (new \yii\db\Query())
                        ->select('id_asocia')
                                    ->from('asociacion')
                                    ->where('id_estado_asocia=1');
        $rows = $activas->all();
                        $command = $activas->createCommand();
                        $row = $command->queryAll();
    }
}
