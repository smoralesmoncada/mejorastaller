<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Alumno;

/**
 * AlumnoSearch represents the model behind the search form of `app\models\Alumno`.
 */
class AlumnoSearch extends Alumno
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['rut_alumno', 'ano_ingreso'], 'integer'],
            [['nombres_alumno', 'apellido_p_alumno', 'apellido_m_alumno', 'carrera_alumno'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Alumno::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'rut_alumno' => $this->rut_alumno,
            'ano_ingreso' => $this->ano_ingreso,
        ]);

        $query->andFilterWhere(['ilike', 'nombres_alumno', $this->nombres_alumno])
            ->andFilterWhere(['ilike', 'apellido_p_alumno', $this->apellido_p_alumno])
            ->andFilterWhere(['ilike', 'apellido_m_alumno', $this->apellido_m_alumno])
            ->andFilterWhere(['ilike', 'carrera_alumno', $this->carrera_alumno]);

        return $dataProvider;
    }
}
