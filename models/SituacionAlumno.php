<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "situacion_alumno".
 *
 * @property int $id_situacion
 * @property string $descripcion_situacion
 *
 * @property Pertenece[] $perteneces
 */
class SituacionAlumno extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'situacion_alumno';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['descripcion_situacion'], 'string', 'max' => 50],
             [['descripcion_situacion'], 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_situacion' => 'Código',
            'descripcion_situacion' => 'Descripción',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerteneces()
    {
        return $this->hasMany(Pertenece::className(), ['id_situacion' => 'id_situacion']);
    }
}
