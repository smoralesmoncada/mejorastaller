<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Asociacion;

/**
 * AsociacionSearch represents the model behind the search form of `app\models\Asociacion`.
 */
class AsociacionSearch extends Asociacion
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_asocia', 'id_plan', 'id_estado_asocia'], 'integer'],
            [['nombre_asocia', 'fecha_inicio_asocia', 'fecha_termino_asocia'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Asociacion::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_asocia' => $this->id_asocia,
            'id_plan' => $this->id_plan,
            'id_estado_asocia' => $this->id_estado_asocia,
            'fecha_inicio_asocia' => $this->fecha_inicio_asocia,
            'fecha_termino_asocia' => $this->fecha_termino_asocia,
        ]);

        $query->andFilterWhere(['ilike', 'nombre_asocia', $this->nombre_asocia]);

        return $dataProvider;
    }
}
