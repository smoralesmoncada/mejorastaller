<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "actividad".
 *
 * @property int $id_act
 * @property string $descripcion_act
 * @property string $fecha_act
 * @property string $nombre_actividad
 *
 * @property Planificacion[] $planificacions
 */
class Actividad extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'actividad';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha_act','descripcion_act','nombre_actividad'],'required'],
            ['fecha_act','mayorque'],
            [['fecha_act'], 'safe'],
            [['descripcion_act'], 'string', 'max' => 500],
            [['nombre_actividad'], 'string', 'max' => 100],
        ];
    }

    public function mayorque($attribute)

    {

       if (date("Y-m-d",strtotime($this->fecha_act)) <= date("Y-m-d"))

          $this->addError($attribute, 'Error, la fecha de la actividad debe ser superior a la de hoy');

    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_act' => 'Código actividad',
            'descripcion_act' => 'Descripción',
            'fecha_act' => 'Fecha',
            'nombre_actividad' => 'Nombre',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlanificacions()
    {
        return $this->hasMany(Planificacion::className(), ['id_act' => 'id_act']);
    }
}
