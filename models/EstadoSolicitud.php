<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "estado_solicitud".
 *
 * @property int $id_estado_sol
 * @property string $nombre_estado_sol
 *
 * @property SolicitudCrearAsociacion[] $solicitudCrearAsociacions
 */
class EstadoSolicitud extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'estado_solicitud';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre_estado_sol'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_estado_sol' => 'Código estado',
            'nombre_estado_sol' => 'Nombre estado',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSolicitudCrearAsociacions()
    {
        return $this->hasMany(SolicitudCrearAsociacion::className(), ['id_estado_sol' => 'id_estado_sol']);
    }
}
