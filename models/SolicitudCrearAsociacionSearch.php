<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SolicitudCrearAsociacion;

/**
 * SolicitudCrearAsociacionSearch represents the model behind the search form of `app\models\SolicitudCrearAsociacion`.
 */
class SolicitudCrearAsociacionSearch extends SolicitudCrearAsociacion
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_solicitud_ca', 'id_asocia', 'id_estado_sol', 'rut_alumno'], 'integer'],
            [['descripcion_ca', 'nombre_ca'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SolicitudCrearAsociacion::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_solicitud_ca' => $this->id_solicitud_ca,
            'id_asocia' => $this->id_asocia,
            'id_estado_sol' => $this->id_estado_sol,
            'rut_alumno' => $this->rut_alumno,
        ]);

        $query->andFilterWhere(['ilike', 'descripcion_ca', $this->descripcion_ca])
            ->andFilterWhere(['ilike', 'nombre_ca', $this->nombre_ca]);

        return $dataProvider;
    }
}
