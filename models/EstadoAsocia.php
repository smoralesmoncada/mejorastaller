<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "estado_asocia".
 *
 * @property int $id_estado_asocia
 * @property string $descripcion_estado_asocia
 *
 * @property Asociacion[] $asociacions
 */
class EstadoAsocia extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'estado_asocia';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['descripcion_estado_asocia'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_estado_asocia' => 'Id Estado Asocia',
            'descripcion_estado_asocia' => 'Descripcion Estado Asocia',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAsociacions()
    {
        return $this->hasMany(Asociacion::className(), ['id_estado_asocia' => 'id_estado_asocia']);
    }
}
