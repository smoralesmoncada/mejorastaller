<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "solicitud_crear_asociacion".
 *
 * @property int $id_solicitud_ca
 * @property int $id_asocia
 * @property int $id_estado_sol
 * @property int $rut_alumno
 * @property string $descripcion_ca
 * @property string $nombre_ca
 *
 * @property Revisa[] $revisas
 * @property UnidadRevision[] $unis
 * @property Alumno $rutAlumno
 * @property Asociacion $asocia
 * @property EstadoSolicitud $estadoSol
 */
class SolicitudCrearAsociacion extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'solicitud_crear_asociacion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_asocia'], 'required'],
            [['descripcion_ca'], 'required'],
            [['nombre_ca'], 'required'],
            [['id_asocia', 'id_estado_sol', 'rut_alumno'], 'default', 'value' => null],
            [['id_asocia', 'id_estado_sol', 'rut_alumno'], 'integer'],
            [['descripcion_ca'], 'string', 'max' => 1000],
            [['nombre_ca'], 'string', 'max' => 50],
            [['rut_alumno'], 'exist', 'skipOnError' => true, 'targetClass' => Alumno::className(), 'targetAttribute' => ['rut_alumno' => 'rut_alumno']],
            [['id_asocia'], 'exist', 'skipOnError' => true, 'targetClass' => Asociacion::className(), 'targetAttribute' => ['id_asocia' => 'id_asocia']],
            [['id_estado_sol'], 'exist', 'skipOnError' => true, 'targetClass' => EstadoSolicitud::className(), 'targetAttribute' => ['id_estado_sol' => 'id_estado_sol']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_solicitud_ca' => 'Código solicitud',
            'id_asocia' => 'Asociación',
            'id_estado_sol' => 'Estado',
            'rut_alumno' => 'Alumno',
            'descripcion_ca' => 'Descripción solicitud',
            'nombre_ca' => 'Asunto',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRevisas()
    {
        return $this->hasMany(Revisa::className(), ['id_solicitud_ca' => 'id_solicitud_ca']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnis()
    {
        return $this->hasMany(UnidadRevision::className(), ['id_uni' => 'id_uni'])->viaTable('revisa', ['id_solicitud_ca' => 'id_solicitud_ca']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRutAlumno()
    {
        return $this->hasOne(Alumno::className(), ['rut_alumno' => 'rut_alumno']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAsocia()
    {
        return $this->hasOne(Asociacion::className(), ['id_asocia' => 'id_asocia']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEstadoSol()
    {
        return $this->hasOne(EstadoSolicitud::className(), ['id_estado_sol' => 'id_estado_sol']);
    }
}
