<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "alumno".
 *
 * @property int $rut_alumno
 * @property string $nombres_alumno
 * @property string $apellido_p_alumno
 * @property string $apellido_m_alumno
 * @property int $ano_ingreso
 * @property string $carrera_alumno
 *
 * @property Denuncia[] $denuncias
 * @property Pertenece[] $perteneces
 * @property SolicitudCrearAsociacion[] $solicitudCrearAsociacions
 */
class Alumno extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'alumno';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['rut_alumno'], 'required'],
            [['rut_alumno', 'ano_ingreso'], 'default', 'value' => null],
            [['rut_alumno', 'ano_ingreso'], 'integer'],
            [['nombres_alumno', 'apellido_p_alumno', 'apellido_m_alumno', 'carrera_alumno'], 'string', 'max' => 30],
            [['rut_alumno'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'rut_alumno' => 'Rut',
            'nombres_alumno' => 'Nombres Alumno',
            'apellido_p_alumno' => 'Apellido Paterno',
            'apellido_m_alumno' => 'Apellido Materno',
            'ano_ingreso' => 'Ano Ingreso',
            'carrera_alumno' => 'Carrera',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDenuncias()
    {
        return $this->hasMany(Denuncia::className(), ['rut_alumno' => 'rut_alumno']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerteneces()
    {
        return $this->hasMany(Pertenece::className(), ['rut_alumno' => 'rut_alumno']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSolicitudCrearAsociacions()
    {
        return $this->hasMany(SolicitudCrearAsociacion::className(), ['rut_alumno' => 'rut_alumno']);
    }

    public function getNombreA() 
    {

        $nombreA = (! empty ( $this->nombres_alumno )) ? $this->nombres_alumno : '';

        $nombreA .= (! empty ( $this->apellido_p_alumno )) ? ((! empty ( $nombreA )) ? " " . $this->apellido_p_alumno : $this->apellido_p_alumno) : '';

        $nombreA .= (! empty ( $this->apellido_m_alumno )) ? ((! empty ( $nombreA )) ? " " . $this->apellido_m_alumno : $this->apellido_m_alumno) : '';

        return $nombreA;
    }
}
