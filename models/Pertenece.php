<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pertenece".
 *
 * @property int $id_situacion
 * @property int $rut_alumno
 * @property int $id_asocia
 * @property int $id_cargo
 *
 * @property Alumno $rutAlumno
 * @property Asociacion $asocia
 * @property Cargo $cargo
 * @property SituacionAlumno $situacion
 */
class Pertenece extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pertenece';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [ 
            [['id_situacion', 'rut_alumno', 'id_asocia', 'id_cargo'], 'required'],
            [['id_situacion', 'rut_alumno', 'id_asocia', 'id_cargo'], 'default', 'value' => null],
            [['id_situacion', 'rut_alumno', 'id_asocia', 'id_cargo'], 'integer'],
            [['rut_alumno', 'id_asocia', 'id_cargo'],'unique'],
            [['id_situacion', 'rut_alumno', 'id_asocia', 'id_cargo'], 'unique', 'targetAttribute' => ['id_situacion', 'rut_alumno', 'id_asocia', 'id_cargo']],
            [['rut_alumno'], 'exist', 'skipOnError' => true, 'targetClass' => Alumno::className(), 'targetAttribute' => ['rut_alumno' => 'rut_alumno']],
            [['id_asocia'], 'exist', 'skipOnError' => true, 'targetClass' => Asociacion::className(), 'targetAttribute' => ['id_asocia' => 'id_asocia']],
            [['id_cargo'], 'exist', 'skipOnError' => true, 'targetClass' => Cargo::className(), 'targetAttribute' => ['id_cargo' => 'id_cargo']],
            [['id_situacion'], 'exist', 'skipOnError' => true, 'targetClass' => SituacionAlumno::className(), 'targetAttribute' => ['id_situacion' => 'id_situacion']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_situacion' => 'Estado alumno',
            'rut_alumno' => 'Alumno',
            'id_asocia' => 'Asociación',
            'id_cargo' => 'Cargo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRutAlumno()
    {
        return $this->hasOne(Alumno::className(), ['rut_alumno' => 'rut_alumno']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAsocia()
    {
        return $this->hasOne(Asociacion::className(), ['id_asocia' => 'id_asocia']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCargo()
    {
        return $this->hasOne(Cargo::className(), ['id_cargo' => 'id_cargo']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSituacion()
    {
        return $this->hasOne(SituacionAlumno::className(), ['id_situacion' => 'id_situacion']);
    }
}
