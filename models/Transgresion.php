<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "transgresion".
 *
 * @property int $id_transgresion
 * @property int $rut_alumno
 * @property int $rut_fun
 * @property int $id_asocia
 * @property string $desc_transgresion
 *
 * @property Alumno $rutAlumno
 * @property Asociacion $asocia
 * @property Funcionario $rutFun
 */
class Transgresion extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'transgresion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['rut_alumno', 'rut_fun', 'id_asocia'], 'default', 'value' => null],
            [['rut_alumno', 'rut_fun', 'id_asocia'], 'integer'],
            [['id_asocia'], 'required'],
            [['desc_transgresion'], 'string', 'max' => 1000],
            [['rut_alumno'], 'exist', 'skipOnError' => true, 'targetClass' => Alumno::className(), 'targetAttribute' => ['rut_alumno' => 'rut_alumno']],
            [['id_asocia'], 'exist', 'skipOnError' => true, 'targetClass' => Asociacion::className(), 'targetAttribute' => ['id_asocia' => 'id_asocia']],
            [['rut_fun'], 'exist', 'skipOnError' => true, 'targetClass' => Funcionario::className(), 'targetAttribute' => ['rut_fun' => 'rut_fun']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_transgresion' => 'Código',
            'rut_alumno' => 'Denunciante 1',
            'rut_fun' => 'Denunciante 2',
            'id_asocia' => 'Asociación',
            'desc_transgresion' => 'Detalle Transgresion',
            'fecha_ denuncia' => 'Fecha ingreso denuncia',
            'fecha_modificacion' => 'Última modificación',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRutAlumno()
    {
        return $this->hasOne(Alumno::className(), ['rut_alumno' => 'rut_alumno']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAsocia()
    {
        return $this->hasOne(Asociacion::className(), ['id_asocia' => 'id_asocia']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRutFun()
    {
        return $this->hasOne(Funcionario::className(), ['rut_fun' => 'rut_fun']);
    }
}
