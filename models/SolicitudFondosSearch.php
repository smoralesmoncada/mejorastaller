<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SolicitudFondos;

/**
 * SolicitudFondosSearch represents the model behind the search form of `app\models\SolicitudFondos`.
 */
class SolicitudFondosSearch extends SolicitudFondos
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_solicitud_f', 'id_asocia', 'monto_f'], 'integer'],
            [['descripcion_f'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SolicitudFondos::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_solicitud_f' => $this->id_solicitud_f,
            'id_asocia' => $this->id_asocia,
            'monto_f' => $this->monto_f,
        ]);

        $query->andFilterWhere(['ilike', 'descripcion_f', $this->descripcion_f]);

        return $dataProvider;
    }
}
