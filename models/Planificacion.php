<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "planificacion".
 *
 * @property int $id_plan
 * @property int $id_act
 * @property string $descripcion_plan
 *
 * @property Asociacion[] $asociacions
 * @property Actividad $act
 */
class Planificacion extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'planificacion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_act'], 'required'],
            [['id_act'], 'default', 'value' => null],
            [['id_act'], 'integer'],
            [['descripcion_plan'], 'string', 'max' => 500],
            [['id_act'], 'exist', 'skipOnError' => true, 'targetClass' => Actividad::className(), 'targetAttribute' => ['id_act' => 'id_act']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_plan' => 'Id Plan',
            'id_act' => 'Id Act',
            'descripcion_plan' => 'Descripcion Plan',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAsociacions()
    {
        return $this->hasMany(Asociacion::className(), ['id_plan' => 'id_plan']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAct()
    {
        return $this->hasOne(Actividad::className(), ['id_act' => 'id_act']);
    }
}
