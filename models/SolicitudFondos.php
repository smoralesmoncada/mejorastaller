<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "solicitud_fondos".
 *
 * @property int $id_solicitud_f
 * @property int $id_asocia
 * @property string $descripcion_f
 * @property int $monto_f
 *
 * @property Asociacion $asocia
 * @property Visualiza[] $visualizas
 * @property UnidadRevision[] $unis
 */
class SolicitudFondos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'solicitud_fondos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_asocia', 'monto_f'], 'default', 'value' => null],
            [['id_asocia', 'monto_f'], 'integer'],
            [['monto_f','descripcion_f'], 'required'],
            [['descripcion_f'], 'string', 'max' => 500],
            [['id_asocia'], 'exist', 'skipOnError' => true, 'targetClass' => Asociacion::className(), 'targetAttribute' => ['id_asocia' => 'id_asocia']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_solicitud_f' => 'Código solicitud',
            'id_asocia' => 'Asociación',
            'descripcion_f' => 'Descripción',
            'monto_f' => 'Monto',
            'id_act' => 'Actividad',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAsocia()
    {
        return $this->hasOne(Asociacion::className(), ['id_asocia' => 'id_asocia']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVisualizas()
    {
        return $this->hasMany(Visualiza::className(), ['id_solicitud_f' => 'id_solicitud_f']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnis()
    {
        return $this->hasMany(UnidadRevision::className(), ['id_uni' => 'id_uni'])->viaTable('visualiza', ['id_solicitud_f' => 'id_solicitud_f']);
    }
}
