<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Actividad;

/**
 * ActividadSearch represents the model behind the search form of `app\models\Actividad`.
 */
class ActividadSearch extends Actividad
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_act'], 'integer'],
            [['descripcion_act', 'fecha_act', 'nombre_actividad'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Actividad::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_act' => $this->id_act,
            'fecha_act' => $this->fecha_act,
        ]);

        $query->andFilterWhere(['ilike', 'descripcion_act', $this->descripcion_act])
            ->andFilterWhere(['ilike', 'nombre_actividad', $this->nombre_actividad]);

        return $dataProvider;
    }
}
