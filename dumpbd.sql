--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.15
-- Dumped by pg_dump version 9.6.15

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: actividad; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.actividad (
    id_act integer NOT NULL,
    descripcion_act character(500),
    fecha_act date
);


--
-- Name: actividad_id_act_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.actividad_id_act_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: actividad_id_act_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.actividad_id_act_seq OWNED BY public.actividad.id_act;


--
-- Name: alumno; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.alumno (
    rut_alumno integer NOT NULL,
    nombres_alumno character varying(30),
    apellido_p_alumno character varying(30),
    apellido_m_alumno character varying(30),
    ano_ingreso integer,
    carrera_alumno character varying(30),
    id integer
);


--
-- Name: asociacion; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.asociacion (
    id_asocia integer NOT NULL,
    id_plan integer,
    id_estado_asocia integer,
    nombre_asocia character(50),
    fecha_inicio_asocia date,
    fecha_termino_asocia date,
    id integer
);


--
-- Name: asociacion_id_asocia_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.asociacion_id_asocia_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: asociacion_id_asocia_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.asociacion_id_asocia_seq OWNED BY public.asociacion.id_asocia;


--
-- Name: auth_assignment; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.auth_assignment (
    item_name character varying(64) NOT NULL,
    user_id character varying(64) NOT NULL,
    created_at integer
);


--
-- Name: auth_item; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.auth_item (
    name character varying(64) NOT NULL,
    type smallint NOT NULL,
    description text,
    rule_name character varying(64),
    data bytea,
    created_at integer,
    updated_at integer
);


--
-- Name: auth_item_child; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.auth_item_child (
    parent character varying(64) NOT NULL,
    child character varying(64) NOT NULL
);


--
-- Name: auth_rule; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.auth_rule (
    name character varying(64) NOT NULL,
    data bytea,
    created_at integer,
    updated_at integer
);


--
-- Name: cargo; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.cargo (
    id_cargo integer NOT NULL,
    descripcion_cargo character(50)
);


--
-- Name: cargo_id_cargo_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.cargo_id_cargo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: cargo_id_cargo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.cargo_id_cargo_seq OWNED BY public.cargo.id_cargo;


--
-- Name: estado_asocia; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.estado_asocia (
    id_estado_asocia integer NOT NULL,
    descripcion_estado_asocia character(50)
);


--
-- Name: estado_asocia_id_estado_asocia_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.estado_asocia_id_estado_asocia_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: estado_asocia_id_estado_asocia_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.estado_asocia_id_estado_asocia_seq OWNED BY public.estado_asocia.id_estado_asocia;


--
-- Name: estado_solicitud; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.estado_solicitud (
    id_estado_sol integer NOT NULL,
    nombre_estado_sol character(50)
);


--
-- Name: estado_solicitud_id_estado_sol_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.estado_solicitud_id_estado_sol_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: estado_solicitud_id_estado_sol_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.estado_solicitud_id_estado_sol_seq OWNED BY public.estado_solicitud.id_estado_sol;


--
-- Name: funcionario; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.funcionario (
    rut_fun integer NOT NULL,
    nombres_fun character varying(30),
    apellido_p_fun character varying(30),
    apellido_m_fun character varying(30),
    cargo_fun character varying(30),
    id integer
);


--
-- Name: migration; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.migration (
    version character varying(180) NOT NULL,
    apply_time integer
);


--
-- Name: pertenece; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.pertenece (
    id_situacion integer NOT NULL,
    rut_alumno integer NOT NULL,
    id_asocia integer NOT NULL,
    id_cargo integer NOT NULL,
    id integer
);


--
-- Name: planificacion; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.planificacion (
    id_plan integer NOT NULL,
    id_act integer NOT NULL,
    descripcion_plan character(500)
);


--
-- Name: planificacion_id_plan_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.planificacion_id_plan_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: planificacion_id_plan_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.planificacion_id_plan_seq OWNED BY public.planificacion.id_plan;


--
-- Name: rendicion_fondo; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.rendicion_fondo (
    id_rendi integer NOT NULL,
    id_asocia integer,
    descripcion_rendi character(500),
    fecha_rendi date,
    adjunto_rendi character(500)
);


--
-- Name: rendicion_fondo_id_rendi_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.rendicion_fondo_id_rendi_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: rendicion_fondo_id_rendi_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.rendicion_fondo_id_rendi_seq OWNED BY public.rendicion_fondo.id_rendi;


--
-- Name: revisa; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.revisa (
    id_solicitud_ca integer NOT NULL,
    id_uni integer NOT NULL
);


--
-- Name: situacion_alumno; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.situacion_alumno (
    id_situacion integer NOT NULL,
    descripcion_situacion character(50)
);


--
-- Name: situacion_alumno_id_situacion_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.situacion_alumno_id_situacion_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: situacion_alumno_id_situacion_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.situacion_alumno_id_situacion_seq OWNED BY public.situacion_alumno.id_situacion;


--
-- Name: solicitud_crear_asociacion; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.solicitud_crear_asociacion (
    id_solicitud_ca integer NOT NULL,
    id_asocia integer NOT NULL,
    id_estado_sol integer,
    rut_alumno integer,
    descripcion_ca character(1000),
    nombre_ca character(50),
    fecha_envio date,
    id integer
);


--
-- Name: solicitud_crear_asociacion_id_solicitud_ca_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.solicitud_crear_asociacion_id_solicitud_ca_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: solicitud_crear_asociacion_id_solicitud_ca_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.solicitud_crear_asociacion_id_solicitud_ca_seq OWNED BY public.solicitud_crear_asociacion.id_solicitud_ca;


--
-- Name: solicitud_fondos; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.solicitud_fondos (
    id_solicitud_f integer NOT NULL,
    id_asocia integer,
    descripcion_f character(500),
    monto_f integer
);


--
-- Name: solicitud_fondos_id_solicitud_f_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.solicitud_fondos_id_solicitud_f_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: solicitud_fondos_id_solicitud_f_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.solicitud_fondos_id_solicitud_f_seq OWNED BY public.solicitud_fondos.id_solicitud_f;


--
-- Name: transgresion; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.transgresion (
    id_transgresion integer NOT NULL,
    rut_alumno integer,
    rut_fun integer,
    id_asocia integer NOT NULL,
    desc_transgresion character(1000),
    id integer,
    fecha_denuncia date,
    fecha_modificacion date
);


--
-- Name: transgresion_id_transgresion_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.transgresion_id_transgresion_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: transgresion_id_transgresion_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.transgresion_id_transgresion_seq OWNED BY public.transgresion.id_transgresion;


--
-- Name: unidad_revision; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.unidad_revision (
    id_uni integer NOT NULL,
    descripcion_uni character(500)
);


--
-- Name: unidad_revision_id_uni_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.unidad_revision_id_uni_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: unidad_revision_id_uni_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.unidad_revision_id_uni_seq OWNED BY public.unidad_revision.id_uni;


--
-- Name: user; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public."user" (
    id integer NOT NULL,
    username character varying(255) NOT NULL,
    auth_key character varying(32) NOT NULL,
    displayname character varying(50),
    password_hash character varying(255) NOT NULL,
    password_reset_token character varying(255),
    email character varying(255) NOT NULL,
    role smallint DEFAULT 1 NOT NULL,
    status smallint DEFAULT 10 NOT NULL,
    created_at integer NOT NULL,
    updated_at integer NOT NULL,
    directiva boolean DEFAULT false
);


--
-- Name: user_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.user_id_seq OWNED BY public."user".id;


--
-- Name: verifica; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.verifica (
    id_rendi integer NOT NULL,
    id_uni integer NOT NULL
);


--
-- Name: visualiza; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.visualiza (
    id_solicitud_f integer NOT NULL,
    id_uni integer NOT NULL,
    estado_sol_fondo character(50)
);


--
-- Name: actividad id_act; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.actividad ALTER COLUMN id_act SET DEFAULT nextval('public.actividad_id_act_seq'::regclass);


--
-- Name: asociacion id_asocia; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.asociacion ALTER COLUMN id_asocia SET DEFAULT nextval('public.asociacion_id_asocia_seq'::regclass);


--
-- Name: cargo id_cargo; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.cargo ALTER COLUMN id_cargo SET DEFAULT nextval('public.cargo_id_cargo_seq'::regclass);


--
-- Name: estado_asocia id_estado_asocia; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.estado_asocia ALTER COLUMN id_estado_asocia SET DEFAULT nextval('public.estado_asocia_id_estado_asocia_seq'::regclass);


--
-- Name: estado_solicitud id_estado_sol; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.estado_solicitud ALTER COLUMN id_estado_sol SET DEFAULT nextval('public.estado_solicitud_id_estado_sol_seq'::regclass);


--
-- Name: planificacion id_plan; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.planificacion ALTER COLUMN id_plan SET DEFAULT nextval('public.planificacion_id_plan_seq'::regclass);


--
-- Name: rendicion_fondo id_rendi; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.rendicion_fondo ALTER COLUMN id_rendi SET DEFAULT nextval('public.rendicion_fondo_id_rendi_seq'::regclass);


--
-- Name: situacion_alumno id_situacion; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.situacion_alumno ALTER COLUMN id_situacion SET DEFAULT nextval('public.situacion_alumno_id_situacion_seq'::regclass);


--
-- Name: solicitud_crear_asociacion id_solicitud_ca; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.solicitud_crear_asociacion ALTER COLUMN id_solicitud_ca SET DEFAULT nextval('public.solicitud_crear_asociacion_id_solicitud_ca_seq'::regclass);


--
-- Name: solicitud_fondos id_solicitud_f; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.solicitud_fondos ALTER COLUMN id_solicitud_f SET DEFAULT nextval('public.solicitud_fondos_id_solicitud_f_seq'::regclass);


--
-- Name: transgresion id_transgresion; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.transgresion ALTER COLUMN id_transgresion SET DEFAULT nextval('public.transgresion_id_transgresion_seq'::regclass);


--
-- Name: unidad_revision id_uni; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.unidad_revision ALTER COLUMN id_uni SET DEFAULT nextval('public.unidad_revision_id_uni_seq'::regclass);


--
-- Name: user id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."user" ALTER COLUMN id SET DEFAULT nextval('public.user_id_seq'::regclass);


--
-- Name: actividad actividad_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.actividad
    ADD CONSTRAINT actividad_pkey PRIMARY KEY (id_act);


--
-- Name: alumno alumno_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.alumno
    ADD CONSTRAINT alumno_pkey PRIMARY KEY (rut_alumno);


--
-- Name: asociacion asociacion_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.asociacion
    ADD CONSTRAINT asociacion_pkey PRIMARY KEY (id_asocia);


--
-- Name: auth_assignment auth_assignment_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.auth_assignment
    ADD CONSTRAINT auth_assignment_pkey PRIMARY KEY (item_name, user_id);


--
-- Name: auth_item_child auth_item_child_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.auth_item_child
    ADD CONSTRAINT auth_item_child_pkey PRIMARY KEY (parent, child);


--
-- Name: auth_item auth_item_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.auth_item
    ADD CONSTRAINT auth_item_pkey PRIMARY KEY (name);


--
-- Name: auth_rule auth_rule_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.auth_rule
    ADD CONSTRAINT auth_rule_pkey PRIMARY KEY (name);


--
-- Name: cargo cargo_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.cargo
    ADD CONSTRAINT cargo_pkey PRIMARY KEY (id_cargo);


--
-- Name: estado_asocia estado_asocia_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.estado_asocia
    ADD CONSTRAINT estado_asocia_pkey PRIMARY KEY (id_estado_asocia);


--
-- Name: estado_solicitud estado_solicitud_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.estado_solicitud
    ADD CONSTRAINT estado_solicitud_pkey PRIMARY KEY (id_estado_sol);


--
-- Name: funcionario funcionario_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.funcionario
    ADD CONSTRAINT funcionario_pkey PRIMARY KEY (rut_fun);


--
-- Name: migration migration_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.migration
    ADD CONSTRAINT migration_pkey PRIMARY KEY (version);


--
-- Name: pertenece pertenece_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.pertenece
    ADD CONSTRAINT pertenece_pkey PRIMARY KEY (id_situacion, rut_alumno, id_asocia, id_cargo);


--
-- Name: planificacion planificacion_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.planificacion
    ADD CONSTRAINT planificacion_pkey PRIMARY KEY (id_plan);


--
-- Name: rendicion_fondo rendicion_fondo_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.rendicion_fondo
    ADD CONSTRAINT rendicion_fondo_pkey PRIMARY KEY (id_rendi);


--
-- Name: revisa revisa_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.revisa
    ADD CONSTRAINT revisa_pkey PRIMARY KEY (id_solicitud_ca, id_uni);


--
-- Name: situacion_alumno situacion_alumno_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.situacion_alumno
    ADD CONSTRAINT situacion_alumno_pkey PRIMARY KEY (id_situacion);


--
-- Name: solicitud_crear_asociacion solicitud_crear_asociacion_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.solicitud_crear_asociacion
    ADD CONSTRAINT solicitud_crear_asociacion_pkey PRIMARY KEY (id_solicitud_ca);


--
-- Name: solicitud_fondos solicitud_fondos_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.solicitud_fondos
    ADD CONSTRAINT solicitud_fondos_pkey PRIMARY KEY (id_solicitud_f);


--
-- Name: transgresion transgresion_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.transgresion
    ADD CONSTRAINT transgresion_pkey PRIMARY KEY (id_transgresion);


--
-- Name: unidad_revision unidad_revision_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.unidad_revision
    ADD CONSTRAINT unidad_revision_pkey PRIMARY KEY (id_uni);


--
-- Name: user user_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (id);


--
-- Name: verifica verifica_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.verifica
    ADD CONSTRAINT verifica_pkey PRIMARY KEY (id_rendi, id_uni);


--
-- Name: visualiza visualiza_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.visualiza
    ADD CONSTRAINT visualiza_pkey PRIMARY KEY (id_solicitud_f, id_uni);


--
-- Name: idx-auth_assignment-user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "idx-auth_assignment-user_id" ON public.auth_assignment USING btree (user_id);


--
-- Name: idx-auth_item-type; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "idx-auth_item-type" ON public.auth_item USING btree (type);


--
-- Name: asociacion asociacion_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.asociacion
    ADD CONSTRAINT asociacion_id_fkey FOREIGN KEY (id) REFERENCES public."user"(id);


--
-- Name: auth_assignment auth_assignment_item_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.auth_assignment
    ADD CONSTRAINT auth_assignment_item_name_fkey FOREIGN KEY (item_name) REFERENCES public.auth_item(name) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: auth_item_child auth_item_child_child_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.auth_item_child
    ADD CONSTRAINT auth_item_child_child_fkey FOREIGN KEY (child) REFERENCES public.auth_item(name) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: auth_item_child auth_item_child_parent_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.auth_item_child
    ADD CONSTRAINT auth_item_child_parent_fkey FOREIGN KEY (parent) REFERENCES public.auth_item(name) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: auth_item auth_item_rule_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.auth_item
    ADD CONSTRAINT auth_item_rule_name_fkey FOREIGN KEY (rule_name) REFERENCES public.auth_rule(name) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: pertenece f3_uni_screa_aso; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.pertenece
    ADD CONSTRAINT f3_uni_screa_aso FOREIGN KEY (id_asocia) REFERENCES public.asociacion(id_asocia);


--
-- Name: pertenece f4_uni_screa_aso; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.pertenece
    ADD CONSTRAINT f4_uni_screa_aso FOREIGN KEY (id_cargo) REFERENCES public.cargo(id_cargo);


--
-- Name: verifica fk2_uni_rendicion; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.verifica
    ADD CONSTRAINT fk2_uni_rendicion FOREIGN KEY (id_uni) REFERENCES public.unidad_revision(id_uni);


--
-- Name: revisa fk2_uni_screa_aso; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.revisa
    ADD CONSTRAINT fk2_uni_screa_aso FOREIGN KEY (id_uni) REFERENCES public.unidad_revision(id_uni);


--
-- Name: pertenece fk2_uni_screa_aso; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.pertenece
    ADD CONSTRAINT fk2_uni_screa_aso FOREIGN KEY (rut_alumno) REFERENCES public.alumno(rut_alumno);


--
-- Name: visualiza fk2_uni_solicitud; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.visualiza
    ADD CONSTRAINT fk2_uni_solicitud FOREIGN KEY (id_uni) REFERENCES public.unidad_revision(id_uni);


--
-- Name: solicitud_crear_asociacion fk_alumno; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.solicitud_crear_asociacion
    ADD CONSTRAINT fk_alumno FOREIGN KEY (rut_alumno) REFERENCES public.alumno(rut_alumno);


--
-- Name: asociacion fk_asoc; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.asociacion
    ADD CONSTRAINT fk_asoc FOREIGN KEY (id_plan) REFERENCES public.planificacion(id_plan);


--
-- Name: asociacion fk_asoc_estado; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.asociacion
    ADD CONSTRAINT fk_asoc_estado FOREIGN KEY (id_estado_asocia) REFERENCES public.estado_asocia(id_estado_asocia);


--
-- Name: solicitud_crear_asociacion fk_asocia; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.solicitud_crear_asociacion
    ADD CONSTRAINT fk_asocia FOREIGN KEY (id_asocia) REFERENCES public.asociacion(id_asocia);


--
-- Name: solicitud_crear_asociacion fk_estado_sol; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.solicitud_crear_asociacion
    ADD CONSTRAINT fk_estado_sol FOREIGN KEY (id_estado_sol) REFERENCES public.estado_solicitud(id_estado_sol);


--
-- Name: alumno fk_iduser; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.alumno
    ADD CONSTRAINT fk_iduser FOREIGN KEY (id) REFERENCES public."user"(id);


--
-- Name: funcionario fk_iduser; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.funcionario
    ADD CONSTRAINT fk_iduser FOREIGN KEY (id) REFERENCES public."user"(id);


--
-- Name: transgresion fk_iduser; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.transgresion
    ADD CONSTRAINT fk_iduser FOREIGN KEY (id) REFERENCES public."user"(id);


--
-- Name: planificacion fk_planifica; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.planificacion
    ADD CONSTRAINT fk_planifica FOREIGN KEY (id_act) REFERENCES public.actividad(id_act);


--
-- Name: rendicion_fondo fk_rendi_fon; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.rendicion_fondo
    ADD CONSTRAINT fk_rendi_fon FOREIGN KEY (id_asocia) REFERENCES public.asociacion(id_asocia);


--
-- Name: solicitud_fondos fk_sol_fondos; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.solicitud_fondos
    ADD CONSTRAINT fk_sol_fondos FOREIGN KEY (id_asocia) REFERENCES public.asociacion(id_asocia);


--
-- Name: verifica fk_uni_rendicion; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.verifica
    ADD CONSTRAINT fk_uni_rendicion FOREIGN KEY (id_rendi) REFERENCES public.rendicion_fondo(id_rendi);


--
-- Name: revisa fk_uni_screa_aso; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.revisa
    ADD CONSTRAINT fk_uni_screa_aso FOREIGN KEY (id_solicitud_ca) REFERENCES public.solicitud_crear_asociacion(id_solicitud_ca);


--
-- Name: pertenece fk_uni_screa_aso; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.pertenece
    ADD CONSTRAINT fk_uni_screa_aso FOREIGN KEY (id_situacion) REFERENCES public.situacion_alumno(id_situacion);


--
-- Name: visualiza fk_uni_solicitud; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.visualiza
    ADD CONSTRAINT fk_uni_solicitud FOREIGN KEY (id_solicitud_f) REFERENCES public.solicitud_fondos(id_solicitud_f);


--
-- Name: pertenece pertenece_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.pertenece
    ADD CONSTRAINT pertenece_id_fkey FOREIGN KEY (id) REFERENCES public."user"(id);


--
-- Name: solicitud_crear_asociacion solicitud_crear_asociacion_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.solicitud_crear_asociacion
    ADD CONSTRAINT solicitud_crear_asociacion_id_fkey FOREIGN KEY (id) REFERENCES public."user"(id);


--
-- Name: transgresion transgresion_id_asocia_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.transgresion
    ADD CONSTRAINT transgresion_id_asocia_fkey FOREIGN KEY (id_asocia) REFERENCES public.asociacion(id_asocia);


--
-- Name: transgresion transgresion_rut_alumno_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.transgresion
    ADD CONSTRAINT transgresion_rut_alumno_fkey FOREIGN KEY (rut_alumno) REFERENCES public.alumno(rut_alumno);


--
-- Name: transgresion transgresion_rut_fun_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.transgresion
    ADD CONSTRAINT transgresion_rut_fun_fkey FOREIGN KEY (rut_fun) REFERENCES public.funcionario(rut_fun);


--
-- PostgreSQL database dump complete
--


insert into public.user (username,auth_key,password_hash,email,role, status, created_at, updated_at,directiva) values
('Samuel','TG_4_Hnpu6MdzpM1OGQ4sJkYtyJZx8aZ','$2y$13$zr24taYaJu94hJo0Qqtkxep639jzb2IK.4aPP/iBdKa/khnLg/81S','samuelmorales_ubb@gmail.com',11,10,1562211634,1562211634,false),
('Andrés','bXtVYQzgvuX6JJjepOQWT7NNn6Ed407p','$2y$13$uQ/KUlrofB2ww7JRJtmaAe.w85u7xaQh1PktgD/fzbNGjmp84A4g2','andresubb@gmail.com',12,10,1563588313,1563588313,false),
('Admin','AjclgV9qcm94wiqYz8L_OX8AWwl8uZ7f','$2y$13$CHCevxw3LQtYdUBmrUbczOEogIMWFOI9gKZpO4ZuRiF7dcjxYLlHW','admin_sistema@gmail.com',10,10,1562222028,1562222028,false),
('Pablo','Ex8hPJYt3_DTgWS_TW4phT-F66KIMsLj','$2y$13$euCQ7s0Ws33AjBT191Hf/uHW6KWBIK5xUdK26u8VW7OC/.UdY.cCi','pablotorres_ubb@gmail.com',11,10,1563591567,1563591567,true),
('Guillermo','icqyqTaIHS4vzw7Xe1kYZrT_ejQPm9D8','$2y$13$g1zQPXsQF2nliFqq.gJXke88h6BMKh.KsUiP4meaJbYm.I7FUEZpi','  guillermobustos_ubb@gmail.com',11,10,1565577781,1565577781,false);

insert into public.alumno (rut_alumno,nombres_alumno, apellido_p_alumno, apellido_m_alumno, ano_ingreso, carrera_alumno, id) values
    (18807479,'Samuel Guillermo', 'Morales', 'Moncada', 2012, 'IECI', 2),
    (14210325,'Guillermo', 'Bustos', 'Valdés', 2013, 'ICInf', 5),
    (18807464,'Pablo', 'Torres', 'Salinas', 2010, 'IECI', 4);

insert into public.cargo(descripcion_cargo) values
    ('Presidente'),
    ('Secretario'),
    ('Tesorero'),
    ('Miembro');

insert into public.estado_asocia(descripcion_estado_asocia) values
    ('Vigente'),
    ('De baja'),
    ('En revisión');

insert into public.situacion_alumno(descripcion_situacion) values
    ('Vigente'),
    ('De baja'),
    ('Pendiente');