<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AlumnoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="alumno-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'rut_alumno') ?>

    <?= $form->field($model, 'nombres_alumno') ?>

    <?= $form->field($model, 'apellido_p_alumno') ?>

    <?= $form->field($model, 'apellido_m_alumno') ?>

    <?= $form->field($model, 'ano_ingreso') ?>

    <?php // echo $form->field($model, 'carrera_alumno') ?>

    <div class="form-group">
        <?= Html::submitButton('Buscar', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
