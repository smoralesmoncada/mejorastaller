<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Alumno */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="alumno-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'rut_alumno')->textInput() ?>

    <?= $form->field($model, 'nombres_alumno')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'apellido_p_alumno')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'apellido_m_alumno')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ano_ingreso')->textInput() ?>

    <?= $form->field($model, 'carrera_alumno')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
