<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<div class="site-error">

    <h1>¡Malas noticias! &#128543</h1>

    <div class="alert alert-danger">
        <?= nl2br(Html::encode($message)) ?>
    </div>

    <p>
        Algo ha salido mal en el servidor mientras procesábamos tu respuesta.
    </p>
    <p>
        Por favor, ponte en contacto con nosotros si el error persiste, muchas gracias!
    </p>

</div>
