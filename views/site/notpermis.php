<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Error de permisos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        No tienes permisos para acceder a esta funcionalidad
    </p>

    <!--<code><?= __FILE__ ?></code>-->
</div>
