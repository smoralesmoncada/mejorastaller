<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Asociacion;


/* @var $this yii\web\View */
/* @var $model app\models\Transgresion */


$cargo = Asociacion::findOne($model->id_asocia);
$this->title = "Denuncia: ".$cargo->nombre_asocia;

//$this->title = $model->id_transgresion;

$this->params['breadcrumbs'][] = ['label' => 'Denuncias', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="transgresion-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->id_transgresion], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->id_transgresion], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Seguro que desea elimiar este dato?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id_transgresion',
            //'rut_alumno',
            //'rut_fun',
            [
                'attribute' => 'id_asocia',
                'value' => function($model){
                    $cargo = Asociacion::findOne($model->id_asocia);
                    return $cargo->nombre_asocia;
                },
            ],
            'desc_transgresion',
            'fecha_denuncia',
            'fecha_modificacion',
        ],
    ]) ?>

</div>
