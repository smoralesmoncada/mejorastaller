<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\models\Alumno;
use app\models\Funcionario;
use app\models\Asociacion;
use dosamigos\datepicker\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Transgresion */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="transgresion-form">

    <?php $form = ActiveForm::begin(); ?>

    <!--<?= $form->field($model, 'rut_alumno')->dropDownList(
        ArrayHelper::map(Alumno::find()->all(),'rut_alumno','nombreA'),
            ['prompt'=>'Seleccione un alumno']
    ) ?>

    <?= $form->field($model, 'rut_fun')->dropDownList(
        ArrayHelper::map(Funcionario::find()->all(),'rut_fun','nombreF'),
            ['prompt'=>'Seleccione un funcionario']
    ) ?>-->

    <?php 
        $vigentes = Yii::$app->db->createCommand(
                'SELECT *
                FROM asociacion
                WHERE id_estado_asocia = 1');
        $consulta = $vigentes->queryAll();
        
        $filas = ArrayHelper::map($consulta,'id_asocia','nombre_asocia');
     ?>

    <?= $form->field($model, 'id_asocia')->dropDownList($filas,['prompt'=>'Seleccione un asociacion']); ?>

    <?= $form->field($model, 'desc_transgresion')->textArea(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
