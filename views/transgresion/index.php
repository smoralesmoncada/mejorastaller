<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Asociacion;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TransgresionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Denuncias';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="transgresion-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Nueva Denuncia', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id_transgresion',
            //'rut_alumno',
            //'rut_fun',
            [
                'attribute' => 'id_asocia',
                'value' => function($model){
                    $cargo = Asociacion::findOne($model->id_asocia);
                    return $cargo->nombre_asocia;
                },
            ],
            'desc_transgresion',
            'fecha_denuncia',
            'fecha_modificacion',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
