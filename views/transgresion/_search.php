<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TransgresionSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="transgresion-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_transgresion') ?>

    <?= $form->field($model, 'rut_alumno') ?>

    <?= $form->field($model, 'rut_fun') ?>

    <?= $form->field($model, 'id_asocia') ?>

    <?= $form->field($model, 'desc_transgresion') ?>

    <div class="form-group">
        <?= Html::submitButton('Buscar', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
