<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Transgresion */

$this->title = 'Nueva Denuncia';
$this->params['breadcrumbs'][] = ['label' => 'Denuncia', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="transgresion-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
