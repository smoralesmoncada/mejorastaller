<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Transgresion */

$this->title = 'Actualizar Denuncia: ' . $model->id_transgresion;
$this->params['breadcrumbs'][] = ['label' => 'Denuncia', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_transgresion, 'url' => ['view', 'id' => $model->id_transgresion]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="transgresion-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
