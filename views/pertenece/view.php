<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Cargo;
use app\models\Asociacion;
use app\models\SituacionAlumno;
use app\models\Alumno; 

/* @var $this yii\web\View */
/* @var $model app\models\Pertenece */


$cargo = Asociacion::findOne($model->id_asocia);
$this->title = "Asociación: ".$cargo->nombre_asocia;

$this->params['breadcrumbs'][] = ['label' => 'Miembros Asociacion', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

\yii\web\YiiAsset::register($this);
?>
<div class="pertenece-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar Informacion', ['update', 'id_situacion' => $model->id_situacion, 'rut_alumno' => $model->rut_alumno, 'id_asocia' => $model->id_asocia, 'id_cargo' => $model->id_cargo], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id_situacion' => $model->id_situacion, 'rut_alumno' => $model->rut_alumno, 'id_asocia' => $model->id_asocia, 'id_cargo' => $model->id_cargo], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Seguro que desea eliminar este dato?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'id_situacion',
                'value' => function($model){
                    $situacion = SituacionAlumno::findOne($model->id_situacion);
                    return $situacion->descripcion_situacion;
                },
            ],
            [
                'attribute' => 'rut_alumno',
                'value' => function($model){
                    $cargo = Alumno::findOne($model->rut_alumno);
                    return $cargo->nombres_alumno.' '.$cargo->apellido_p_alumno.' '.$cargo->apellido_m_alumno;
                },
            ],
            [
                'attribute' => 'id_asocia',
                'value' => function($model){
                    $cargo = Asociacion::findOne($model->id_asocia);
                    return $cargo->nombre_asocia;
                },
            ],
            [
                'attribute' => 'id_cargo',
                'value' => function($model){
                    $cargo = Cargo::findOne($model->id_cargo);
                    return $cargo->descripcion_cargo;
                },
            ],
        ],
    ]) ?>

</div>
