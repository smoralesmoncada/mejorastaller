<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Pertenece */

$this->title = 'Agregar Nuevo Miembro';
$this->params['breadcrumbs'][] = ['label' => 'Nuevo Miembro', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pertenece-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
