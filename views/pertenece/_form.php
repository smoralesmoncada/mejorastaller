<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\models\Alumno;
use app\models\Asociacion;
use app\models\Cargo;
use app\models\SituacionAlumno;
use dosamigos\datepicker\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Pertenece */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pertenece-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'rut_alumno')->dropDownList(
        ArrayHelper::map(Alumno::find()->all(),'rut_alumno','nombreA'),
            ['prompt'=>'Seleccione un alumno']
    ) ?>
 
    <?= $form->field($model, 'id_situacion')->dropDownList(
        ArrayHelper::map(SituacionAlumno::find()->all(),'id_situacion','descripcion_situacion'),
            ['prompt'=>'Seleccione una situación']
    ) ?>

    <?= $form->field($model, 'id_asocia')->dropDownList(
        ArrayHelper::map(Asociacion::find()->all(),'id_asocia','nombre_asocia'),
            ['prompt'=>'Seleccione una asociacion']
    ) ?>

    <?= $form->field($model, 'id_cargo')->dropDownList(
        ArrayHelper::map(Cargo::find()->all(),'id_cargo','descripcion_cargo'),
            ['prompt'=>'Seleccione un cargo']
    ) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
