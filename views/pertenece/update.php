<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Pertenece */

$this->title = 'Actualizar Datos';
$this->params['breadcrumbs'][] = ['label' => 'Perteneces', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_situacion, 'url' => ['view', 'id_situacion' => $model->id_situacion, 'rut_alumno' => $model->rut_alumno, 'id_asocia' => $model->id_asocia, 'id_cargo' => $model->id_cargo]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pertenece-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
