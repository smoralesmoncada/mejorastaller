<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Cargo;
use app\models\Asociacion;
use app\models\SituacionAlumno;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PerteneceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Miembros Asociacion';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pertenece-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'id_situacion',
                'value' => function($model){
                    $situacion = SituacionAlumno::findOne($model->id_situacion);
                    return $situacion->descripcion_situacion;
                },
            ],
            //'rut_alumno',
            [
                'attribute' => 'id_asocia',
                'value' => function($model){
                    $cargo = Asociacion::findOne($model->id_asocia);
                    return $cargo->nombre_asocia;
                },
            ],
            [
                'attribute' => 'id_cargo',
                'value' => function($model){
                    $cargo = Cargo::findOne($model->id_cargo);
                    return $cargo->descripcion_cargo;
                },
            ],

            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
