<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SolicitudCrearAsociacion */

$this->title = 'Crear Solicitud de asociación';
$this->params['breadcrumbs'][] = ['label' => 'Solicitud Crear Asociaciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="solicitud-crear-asociacion-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
