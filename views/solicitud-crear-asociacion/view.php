<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Asociacion;
use app\models\EstadoSolicitud;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\SolicitudCrearAsociacion */


$cargo = Asociacion::findOne($model->id_asocia);
$this->title = "Solicitud de asociación: ".$cargo->nombre_asocia;

//$this->title = $model->id_solicitud_ca;

$this->params['breadcrumbs'][] = ['label' => 'Solicitud Crear Asociaciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="solicitud-crear-asociacion-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Modificar', ['update', 'id' => $model->id_solicitud_ca], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->id_solicitud_ca], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Seguro que deseas eliminar este dato?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id_solicitud_ca',  
            [
                'attribute' => 'id_asocia',
                'value' => function($model){
                    $asociacion = Asociacion::findOne($model->id_asocia);
                    return $asociacion->nombre_asocia;
                },
               'filter' => ArrayHelper::map(Asociacion::find()->all(),'id_asocia','nombre_asocia'),
            ],
            [
                'attribute' => 'id_estado_sol',
                'value' => function($model){
                    $asociacion = EstadoSolicitud::findOne($model->id_estado_sol);
                    return $asociacion->nombre_estado_sol;
                },
               'filter' => ArrayHelper::map(EstadoSolicitud::find()->all(),'id_estado_sol','nombre_estado_sol'),
            ],
            //'rut_alumno',
            'descripcion_ca',
            'nombre_ca',
        ],
    ]) ?>

</div>
