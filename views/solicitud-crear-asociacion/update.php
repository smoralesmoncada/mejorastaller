<?php

use yii\helpers\Html;
use app\models\Asociacion;

/* @var $this yii\web\View */
/* @var $model app\models\SolicitudCrearAsociacion */

$cargo = Asociacion::findOne($model->id_asocia);
$this->title = "Modificando asociación: ".$cargo->nombre_asocia;

//$this->title = 'Modificar solicitud asociación: ' . $model->id_solicitud_ca;
$this->params['breadcrumbs'][] = ['label' => 'Solicitud Crear Asociaciones', 'url' => ['index']];

$this->params['breadcrumbs'][] = ['label' => $model->id_solicitud_ca, 'url' => ['view', 'id' => $model->id_solicitud_ca]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="solicitud-crear-asociacion-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
