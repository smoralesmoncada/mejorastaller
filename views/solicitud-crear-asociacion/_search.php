<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SolicitudCrearAsociacionSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="solicitud-crear-asociacion-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_solicitud_ca') ?>

    <?= $form->field($model, 'id_asocia') ?>

    <?= $form->field($model, 'id_estado_sol') ?>

    <?= $form->field($model, 'rut_alumno') ?>

    <?= $form->field($model, 'descripcion_ca') ?>

    <?php // echo $form->field($model, 'nombre_ca') ?>

    <div class="form-group">
        <?= Html::submitButton('Buscar', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
