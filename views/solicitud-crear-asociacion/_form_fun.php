<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\models\Asociacion;
use app\models\EstadoSolicitud;
use app\models\Alumno;

/* @var $this yii\web\View */
/* @var $model app\models\SolicitudCrearAsociacion */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="solicitud-crear-asociacion-form">

    <?php $form = ActiveForm::begin(); ?>

    <!--<?= $form->field($model, 'id_asocia')->textInput() ?>

    <?= $form->field($model, 'id_asocia')->dropDownList(
        ArrayHelper::map(asociacion::find()->all(),'id_asocia','nombre_asocia'),
            ['prompt'=>'Seleccione una asociacion']
    ) ?>

-->
<?php   $cargo = Asociacion::findOne($model->id_asocia); ?>
    <?= $form->field($cargo, 'nombre_asocia')->textInput(['readonly' => true])?>
<!--

    <?= $form->field($model, 'id_estado_sol')->textInput() ?>-->

    <?= $form->field($model, 'id_estado_sol')->dropDownList(
        ArrayHelper::map(estadosolicitud::find()->all(),'id_estado_sol','nombre_estado_sol'),
            ['prompt'=>'Seleccione un estado']
    ) ?>
<!--
    <?= $form->field($model, 'rut_alumno')->dropDownList(
        ArrayHelper::map(Alumno::find()->all(),'rut_alumno','nombreA'),
            ['prompt'=>'Seleccione un alumno']
    ) ?>-->

    <?= $form->field($model, 'descripcion_ca')->textArea(['maxlength' => true]) ?>

    <?= $form->field($model, 'nombre_ca')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar solicitud', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
