<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use app\models\Asociacion;
use app\models\EstadoSolicitud;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SolicitudCrearAsociacionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = 'Solicitudes de Asociación';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="solicitud-crear-asociacion-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <!--<?= Html::a('Nueva Solicitud', ['create'], ['class' => 'btn btn-success']) ?>-->
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id_solicitud_ca',
            [
                'attribute' => 'id_asocia',
                'value' => function($model){
                    $asociacion = Asociacion::findOne($model->id_asocia);
                    return $asociacion->nombre_asocia;
                },
                //'filter' => ArrayHelper::map(Asociacion::find()->all(),'id_asocia','nombre_asocia'),
            ],
            
            [
                'attribute' => 'id_estado_sol', 
                'value' => function($model){
                    $asociacion = EstadoSolicitud::findOne($model->id_estado_sol);
                    return $asociacion->nombre_estado_sol;
                },
               'filter' => ArrayHelper::map(EstadoSolicitud::find()->all(),'id_estado_sol','nombre_estado_sol'),
            ],

            //'rut_alumno',
            'descripcion_ca',
            'nombre_ca',
            'fecha_envio',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
