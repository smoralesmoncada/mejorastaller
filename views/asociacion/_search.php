<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AsociacionSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="asociacion-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_asocia') ?>

    <?= $form->field($model, 'id_plan') ?>

    <?= $form->field($model, 'id_estado_asocia') ?>

    <?= $form->field($model, 'nombre_asocia') ?>

    <?= $form->field($model, 'fecha_inicio_asocia') ?>

    <?php // echo $form->field($model, 'fecha_termino_asocia') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
