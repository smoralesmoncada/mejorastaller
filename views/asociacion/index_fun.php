<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\EstadoAsocia;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AsociacionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Asociaciones';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="asociacion-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <!--<?= Html::a('Crear nueva', ['create'], ['class' => 'btn btn-success']) ?>-->
         <?= Html::a('Ver solicitudes enviadas', ['/solicitud-crear-asociacion/index'], ['class' => 'btn btn-info']) ?>
    </p>


    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id_asocia',
            //'id_plan',

            [
                'attribute' => 'id_estado_asocia',
                'value' => function($model){
                    $asociacion = EstadoAsocia::findOne($model->id_estado_asocia);
                    return $asociacion->descripcion_estado_asocia;
                },
               'filter' => ArrayHelper::map(EstadoAsocia::find()->all(),'id_estado_asocia','descripcion_estado_asocia'),
            ],
            'nombre_asocia',
            'fecha_inicio_asocia',
            'fecha_termino_asocia',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
