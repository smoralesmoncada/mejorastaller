<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\models\EstadoAsocia;
use dosamigos\datepicker\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Asociacion */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="asociacion-form">

    <?php $form = ActiveForm::begin(); ?>

    <!--<?= $form->field($model, 'id_plan')->textInput() ?>-->

    <?= $form->field($model, 'id_estado_asocia')->dropDownList(
        ArrayHelper::map(EstadoAsocia::find()->all(),'id_estado_asocia','descripcion_estado_asocia'),
            ['prompt'=>'Seleccione un estado']
    ) ?>

    <?= $form->field($model, 'nombre_asocia')->textInput(['maxlength' => true]) ?>

    <!--<?= $form->field($model, 'fecha_inicio_asocia')->textInput() ?>-->

    <!--<table>
        
        <tr>
            <td>
                
            </td>
            <td>
                
            </td>
        </tr>
    </table>
    -->
    
    <?= $form->field($model, 'fecha_inicio_asocia')->widget(
    DatePicker::className(), [
        // inline too, not bad
         'inline' => true, 
         // modify template for custom rendering
        'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
        'clientOptions' => [
            'autoclose' => true,
            'format' => 'dd-M-yyyy'
        ]
    ]);?>

    <?= $form->field($model, 'fecha_termino_asocia')->widget(
    DatePicker::className(), [
        // inline too, not bad
         'inline' => true, 
         // modify template for custom rendering
        'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
        'clientOptions' => [
            'autoclose' => true,
            'format' => 'dd-M-yyyy'
        ]
    ]);?>

<!--
    <?php echo '<label>Fecha de inicio</label>' ?>
    <?= DatePicker::widget([
    'model' => $model,
    'attribute' => 'fecha_inicio_asocia',
    'template' => '{addon}{input}',
        'clientOptions' => [
            'autoclose' => true,
            'format' => 'dd-M-yyyy'
        ]
]);?>

    <?= $form->field($model, 'fecha_termino_asocia')->textInput() ?>

    <?php echo '<label>Fecha de término</label>' ?>
    <?= DatePicker::widget([
    'model' => $model,
    'attribute' => 'fecha_termino_asocia',
    'template' => '{addon}{input}',
        'clientOptions' => [
            'autoclose' => true,
            'format' => 'dd-M-yyyy'
        ]
]);?>-->
    <br>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
