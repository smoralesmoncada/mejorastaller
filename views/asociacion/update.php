<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Asociacion */

$this->title = 'Modificar Asociación: ' . $model->nombre_asocia;
$this->params['breadcrumbs'][] = ['label' => 'Asociacions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_asocia, 'url' => ['view', 'id' => $model->id_asocia]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="asociacion-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
