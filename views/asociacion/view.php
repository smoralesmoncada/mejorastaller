<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\EstadoAsocia;

/* @var $this yii\web\View */
/* @var $model app\models\Asociacion */

$this->title = "Asociación: ".$model->nombre_asocia;
$this->params['breadcrumbs'][] = ['label' => 'Asociaciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="asociacion-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Modificar', ['update', 'id' => $model->id_asocia], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->id_asocia], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Seguro que deseas eliminar este dato?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id_asocia',
            //'id_plan',
            [
                'attribute' => 'id_estado_asocia',
                'value' => function($model){
                    $cargo = EstadoAsocia::findOne($model->id_estado_asocia);
                    return $cargo->descripcion_estado_asocia;
                },
            ],
            'nombre_asocia',
            'fecha_inicio_asocia',
            'fecha_termino_asocia',
        ],
    ]) ?>
    <?= Html::a('Ir a formulario de solicitud', ['solicitud-crear-asociacion/create', 'id' => $model->id_asocia], ['class' => 'btn btn-success']) ?>

</div>
