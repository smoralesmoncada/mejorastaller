<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'Proyecto Taller',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    if (!Yii::$app->user->isGuest) {
        if (Yii::$app->user->identity->role == 10) {  //Si es administrador del sistema, tiene todas las funcionalidades
            # code...
        echo Nav::widget([
            
                'options' => ['class' => 'navbar-nav navbar-right'],
            'items' => [
                ['label' => 'Inicio', 'url' => ['/site/index']],
                ['label' => 'Transgresiones', 'url' => ['/transgresion/index']],
                ['label' => 'Solicitud Asociacion', 'url' => ['/solicitud-crear-asociacion/index']],
                ['label' => 'Solicitud Fondos', 'url' => ['/solicitud-fondos/index']],
                ['label' => 'Asociaciones', 'url' => ['/asociacion/index']],
                ['label' => 'Agregar miembros', 'url' => ['/pertenece/index']],
                ['label' => 'Agregar usuarios', 'url' => ['/site/signup']],
                //['label' => 'About', 'url' => ['/site/about']],
                //['label' => 'Contact', 'url' => ['/site/contact']],
                Yii::$app->user->isGuest ? (
                    ['label' => 'Login', 'url' => ['/site/login']]
                ) : (
                    '<li>'
                    . Html::beginForm(['/site/logout'], 'post')
                    . Html::submitButton(
                        'Logout (' . Yii::$app->user->identity->username . ')',
                        ['class' => 'btn btn-link logout']
                    )
                    . Html::endForm()
                    . '</li>'
                ) 
            ],
            
        ]);
        }
        if (Yii::$app->user->identity->role == 11 && Yii::$app->user->identity->directiva == false) { //Si es estudiante tiene estas funcionalidades
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => [
                    ['label' => 'Inicio', 'url' => ['/site/index']],
                    ['label' => 'Registrar asociación', 'url' => ['/asociacion/index']],
                    ['label' => 'Mis asociaciones', 'url' => ['/pertenece/index']],
                    //['label' => 'Solicitud Asociacion', 'url' => ['/solicitud-crear-asociacion/index']],
                    ['label' => 'Agregar actividad', 'url' => ['/actividad/index']],
                    ['label' => 'Mis denuncias', 'url' => ['/transgresion/index']],
                    Yii::$app->user->isGuest ? (
                    ['label' => 'Login', 'url' => ['/site/login']]
                ) : (
                        '<li>'
                        . Html::beginForm(['/site/logout'], 'post')
                        . Html::submitButton(
                            'Logout (' . Yii::$app->user->identity->username . ')',
                            ['class' => 'btn btn-link logout']
                        )
                        . Html::endForm()
                        . '</li>'
                    ) 
                ],
            
            ]);
        }
        if (Yii::$app->user->identity->role == 11 && Yii::$app->user->identity->directiva == true) { //Si es directiva tiene estas funcionalidades
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => [
                    ['label' => 'Inicio', 'url' => ['/site/index']],
                    ['label' => 'Mis asociaciones', 'url' => ['/asociacion/index']],
                    ['label' => 'Solicitud Asociacion', 'url' => ['/solicitud-crear-asociacion/index']],
                    ['label' => 'Gestionar miembros', 'url' => ['/pertenece/index']],
                    ['label' => 'Solicitud Fondos', 'url' => ['/solicitud-fondos/index']],
                    ['label' => 'Mis denuncias', 'url' => ['/transgresion/index']],
                    Yii::$app->user->isGuest ? (
                    ['label' => 'Login', 'url' => ['/site/login']]
                ) : (
                        '<li>'
                        . Html::beginForm(['/site/logout'], 'post')
                        . Html::submitButton(
                            'Logout (' . Yii::$app->user->identity->username . ')',
                            ['class' => 'btn btn-link logout']
                        )
                        . Html::endForm()
                        . '</li>'
                    ) 
                ],
            
            ]);
        }
        if (Yii::$app->user->identity->role == 12) { //Si es funcionario ubb tiene estas funcionalidades
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => [
                    ['label' => 'Inicio', 'url' => ['/site/index']],
                    ['label' => 'Asociaciones', 'url' => ['/asociacion/index']],
                    ['label' => 'Agregar directiva', 'url' => ['/pertenece/index']],
                    ['label' => 'Solicitud Asociacion', 'url' => ['/solicitud-crear-asociacion/index']],
                    ['label' => 'Mis denuncias', 'url' => ['/transgresion/index']],
                    Yii::$app->user->isGuest ? (
                    ['label' => 'Login', 'url' => ['/site/login']]
                ) : (
                        '<li>'
                        . Html::beginForm(['/site/logout'], 'post')
                        . Html::submitButton(
                            'Logout (' . Yii::$app->user->identity->username . ')',
                            ['class' => 'btn btn-link logout']
                        )
                        . Html::endForm()
                        . '</li>'
                    ) 
                ],
            
            ]);
        }
    }
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; Taller de desarrollo de software <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
