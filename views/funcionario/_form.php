<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Funcionario */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="funcionario-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'rut_fun')->textInput() ?>

    <?= $form->field($model, 'nombres_fun')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'apellido_p_fun')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'apellido_m_fun')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cargo_fun')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
