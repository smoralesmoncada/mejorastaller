<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SituacionAlumnoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Situación alumno';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="situacion-alumno-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear nuevo', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            'id_situacion',
            'descripcion_situacion',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
