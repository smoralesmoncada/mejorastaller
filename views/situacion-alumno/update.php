<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SituacionAlumno */

$this->title = 'Modificar dato: ' . $model->id_situacion;
$this->params['breadcrumbs'][] = ['label' => 'Situación alumno', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_situacion, 'url' => ['view', 'id' => $model->id_situacion]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="situacion-alumno-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
