<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SituacionAlumno */

$this->title = 'Nueva situación';
$this->params['breadcrumbs'][] = ['label' => 'Situacion Alumnos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="situacion-alumno-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
