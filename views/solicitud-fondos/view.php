<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Actividad;

/* @var $this yii\web\View */
/* @var $model app\models\SolicitudFondos */

$this->title = 'Solicitud de fondos';
$this->params['breadcrumbs'][] = ['label' => 'Solicitud Fondos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="solicitud-fondos-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Modificar', ['update', 'id' => $model->id_solicitud_f], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->id_solicitud_f], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Seguro que desea elimiar este dato?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id_solicitud_f',
            //'id_asocia',
            [
                'attribute' => 'id_act',
                'value' => function($model){
                    $situacion = Actividad::findOne($model->id_act);
                    return $situacion->nombre_actividad;
                },
            ],
            'descripcion_f',
            'monto_f',
            'estado',
            //'id_act',
        ],
    ]) ?>

</div>
