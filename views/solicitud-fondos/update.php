<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SolicitudFondos */

$this->title = 'Modificar solicitud: ';
$this->params['breadcrumbs'][] = ['label' => 'Solicitud Fondos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_solicitud_f, 'url' => ['view', 'id' => $model->id_solicitud_f]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="solicitud-fondos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
