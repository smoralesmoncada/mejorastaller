<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Actividad;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SolicitudFondosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Solicitudes de fondos económicos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="solicitud-fondos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <!--<?= Html::a('Nueva solicitud', ['create'], ['class' => 'btn btn-success']) ?>-->
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id_solicitud_f',
            //'id_asocia',
            [
                'attribute' => 'id_act',
                'value' => function($model){
                    $situacion = Actividad::findOne($model->id_act);
                    return $situacion->nombre_actividad;
                },
            ],
            'descripcion_f',
            'monto_f',
            'estado',
            //'id_act', 

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
