<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\money\MaskMoney;

/* @var $this yii\web\View */
/* @var $model app\models\SolicitudFondos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="solicitud-fondos-form">

    <?php $form = ActiveForm::begin(); ?>

    <!--<?= $form->field($model, 'id_asocia')->textInput() ?>-->

    <?= $form->field($model, 'descripcion_f')->textArea(['maxlength' => true]) ?>

    <?php echo "Ingrese el monto requerido (sin puntos)"; ?>
    <?= $form->field($model, 'monto_f')->textInput() ?>

    <?php $a= ['Aprobada','Rechazada']; ?>
    <?php echo $form->field($model, 'estado')->dropDownList($a,['prompt'=>'Seleccione un estado']); ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
