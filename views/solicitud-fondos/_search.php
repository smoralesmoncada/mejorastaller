<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SolicitudFondosSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="solicitud-fondos-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_solicitud_f') ?>

    <?= $form->field($model, 'id_asocia') ?>

    <?= $form->field($model, 'descripcion_f') ?>

    <?= $form->field($model, 'monto_f') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
