<?php
use yii\base\InvalidConfigException;
use yii\db\Schema;
use yii\rbac\DbManager;
use yii\db\Migration;

/**
 * Class m190704_054823_init_rbac
 */
class m190704_054823_init_rbac extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;

        // Puede crear
        $createSituacion = $auth->createPermission('situacion-Alumno/create');
        $createSituacion->description = 'Crea una situación al estudiante';
        $auth->add($createSituacion);

        // Puede editar
        $updateSituacion = $auth->createPermission('updateSituacion');
        $updateSituacion->description = 'Actualiza una situacion';
        $auth->add($updateSituacion);

        // Puede ver
        $ViewSituacion = $auth->createPermission('viewSituacion');
        $ViewSituacion->description = 'Visualiza una situación al estudiante';
        $auth->add($ViewSituacion);

        // Puede eliminar
        $deleteSituacion = $auth->createPermission('deleteSituacion');
        $deleteSituacion->description = 'Elimina una situacion';
        $auth->add($deleteSituacion);

        $User = $auth->createRole('User');
        $auth->add($User);
        $auth->addChild($User, $createSituacion);
        $auth->addChild($User, $ViewSituacion);

        $Admin = $auth->createRole('Admin');
        $auth->add($Admin);
        $auth->addChild($Admin, $updateSituacion);
        $auth->addChild($Admin, $deleteSituacion);
        $auth->addChild($Admin, $User);

        $auth->assign($User, 2);
        $auth->assign($Admin, 1);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190704_054823_init_rbac cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190704_054823_init_rbac cannot be reverted.\n";

        return false;
    }
    */
}
