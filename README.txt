﻿Requisitos:
- Apache2
- php7.0
- PostgreSQL9.6.15
- php-pgsql

Instrucciones: 

Desde Linux:

- Instalar los programas necesarios desde la terminal con el comando "sudo apt-get install (programa)" donde (programa) es el programa a instalar. Los programas necesarios son: apache2, php7.0, postgresql y luego instalar la extension php-pgsql con el mismo comando.
- Dirigirse con la terminal a la direccion "/etc/php/7.0/apache2" con el comando "cd /etc/php/7.0/apache2".
- Editar el archivo "php.ini" con el comando "sudo nano php.ini" y descomentar la linea "extension=pdo_pgsql".
- Reiniciar Apache2 desde la terminal con el comando "sudo service apache2 restart".
- Mover la carpeta del proyecto a la direccion "var/www/html". Para esto dirigirse en la consola a la direccion en donde se descargo el proyecto, generalmente en la carpeta "Descargas" con el comando "cd (ubicacion)" donde (ubicacion) es donde se encuentra actualmente el proyecto y luego usar el comando "sudo mv taller /var/www/html", donde taller es el nombre de la carpeta del proyecto.
- Dar permisos de escritura a la carpeta "web/assets/" ubicados en la carpeta del proyecto con los comandos "sudo chmod 777 taller", "sudo chmod 777 taller/web" y "sudo chmod 777 taller/web/assets" donde taller es el nombre de la carpeta del proyecto.

Desde Windows 10: 

- Descargar Apache2 en su versión actual 2.4.28 VC15 desde el siguiente enlace: "https://www.apachelounge.com/download/" . Descomprimir el archivo y copiarlo en la caperta raiz de C.
- Descargar e instalar Microsoft Visual C ++ 2012 Runtime, necesario para ejecutar correctamente Apache desde el siguiente enlace: "https://support.microsoft.com/en-us/help/2977003/the-latest-supported-visual-c-downloads".
- Dirigirse a la direccion "C:\Apache24\conf" y abrir el archivo "httpd.conf" con cualquier editor de texto y realizar los siguientes cambios:
	- En la línea Listen establecer *.80
	- En la línea ServerName especificar localhost:80
	- En la línea AllowOverride cambiar none por All
- Establecemos Apache como un servicio de Windows. Para ello, abrimos la consola de comandos e ingresamos lo siguiente:
	- "cd C:\Apache24\bin"
	- "httpd -k install"
- Descargar php en su version mas reciente desde el siguiente enlace: "https://windows.php.net/download/".
- Extraeremos el contenido en una carpeta llamada "php" y la pegaremos en la raiz de C.
- Ingresamos a "Panel de control\Sistema y seguridad\Sistema" y presionamos en "Configuración avanzada del sistema". Allí pulsamos el botón "Variables de entorno" y en la nueva ventana vamos a la sección "Variables del sistema" ubicamos la línea "Path" y pulsamos en el botón "Editar".
- En la ventana desplegada pulsamos en el botón "Nuevo" y añadiremos la línea "C:\php"
- Reiniciar el sistema para que se apliquen los cambios.
- Abrir nuevamente el archivo "httpd.conf" que se encuentra en "C:\Apache24\conf" e ingresar las siguientes lineas:
	LoadModule php5_module “c:/php/php5apache2_4.dll”
	AddHandler application/x-httpd-php .php
	PHPIniDir “C:/php”
- Descargar e instalar PostgreSQL desde el siguiente enlace: "https://www.postgresql.org/download/windows/".
- Mover la carpeta del proyecto a la direccion "C:\Apache24\htdocs"

Configuración base de datos:
- Ejecutar "dumpbd.sql" en la base de datos que usará el sistema.
- Abrir el archivo "db.php" ubicado en la carpeta "config" del proyecto con un editor de texto y editar las siguientes lineas según sea necesario:
	- "'dsn' => 'pgsql:host=localhost;dbname=(nombre)'", si ejecuta la bd de forma local, o "'dsn' => 'pgsql:host=(direccion);port=(puerto);dbname=(nombre)'", donde (direccion) es la direccion donde se encuentra la base de datos, (puerto) es el puerto en que se ejecuta la base de datos, en el caso de PostgreSQL suele ser el puerto 5432 y (nombre) es el nombre de la base de datos.
	- "'username' => '(nombreusuario)'," donde (nombreusuario) es el nombre de usuario en la base de datos 
	- "'password' => '(pass)'," donde pass es la contraseña de la bd

Usuarios de acceso para probar el sistema:
Estudiantes
		username	password
		Samuel		qwe123qwe
		Pablo 		nuevo123 (directiva)
		Guillermo	guillermo123
	
	Funcionario ubb (Entidad)	
		username	password	
		Andrés		andres123
	
	Admin sistema
		username	password
		Admin		qwe123qwe
