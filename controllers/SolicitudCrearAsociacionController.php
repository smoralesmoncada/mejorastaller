<?php

namespace app\controllers;

use Yii;
use app\models\SolicitudCrearAsociacion;
use app\models\LoginForm;
use app\models\SolicitudCrearAsociacionSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;

/**
 * SolicitudCrearAsociacionController implements the CRUD actions for SolicitudCrearAsociacion model.
 */
class SolicitudCrearAsociacionController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all SolicitudCrearAsociacion models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (Yii::$app->user->isGuest) {
            if (!Yii::$app->user->isGuest) {
                return $this->goHome();
            } 
            $model = new LoginForm();
            if ($model->load(Yii::$app->request->post()) && $model->login()) {
                return $this->goBack();
            }
            $model->password = '';
            return $this->render('../site/login', [
                'model' => $model,
            ]);
            }else{
                if (Yii::$app->user->identity->role == 11) {
                $usuario = Yii::$app->user->identity->id;
                    $query = SolicitudCrearAsociacion::find()->where(['id' => $usuario]);
                    $provider = new ActiveDataProvider([
                        'query' => $query,
                        'pagination' => [
                            'pageSize' => 10,
                        ],
                        
                    ]);

                    return $this->render('index', [
                        //'searchModel' => $searchModel,
                        'dataProvider' => $provider,
                    ]);
                }
                if (Yii::$app->user->identity->role == 12) {
                    $searchModel = new SolicitudCrearAsociacionSearch();
                    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

                    return $this->render('index_fun', [
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider,
                    ]);


                }else{
                    return $this->render('../site/notpermis');
                }
            }   
    }

    /**
     * Displays a single SolicitudCrearAsociacion model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        if (Yii::$app->user->isGuest) {
            if (!Yii::$app->user->isGuest) {
                return $this->goHome();
            } 
            $model = new LoginForm();
            if ($model->load(Yii::$app->request->post()) && $model->login()) {
                return $this->goBack();
            }
            $model->password = '';
            return $this->render('../site/login', [
                'model' => $model,
            ]);
            }else{
                if (Yii::$app->user->identity->role == 12) {
                	$model = $this->findModel($id);
                	$conection=Yii::$app->db;
                	if ($model->id_estado_sol == 2) {

                		$conection->createCommand()->update('asociacion', ['id_estado_asocia'=>1], 'id_asocia='.$model->id_asocia)->execute();
                	}
                    if ($model->id_estado_sol == 1) {

                        $conection->createCommand()->update('asociacion', ['id_estado_asocia'=>3], 'id_asocia='.$model->id_asocia)->execute();
                    }
                    return $this->render('view', [
                        'model' => $this->findModel($id),
                    ]);
                }
                if (Yii::$app->user->identity->role == 11) {
                    $asociacion = $this->findModel($id);      
                    if (Yii::$app->user->identity->id == $asociacion->id) {
                        return $this->render('view', [
                            'model' => $this->findModel($id),
                        ]);
                }else{
                    return $this->render('../site/notpermis');
                } 
                }else{
                    return $this->render('../site/notpermis');
                }
            }
    }

    /**
     * Creates a new SolicitudCrearAsociacion model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id)
    {
        if (Yii::$app->user->isGuest) {
            if (!Yii::$app->user->isGuest) {
                return $this->goHome();
            } 
            $model = new LoginForm();
            if ($model->load(Yii::$app->request->post()) && $model->login()) {
                return $this->goBack();
            }
            $model->password = '';
            return $this->render('../site/login', [
                'model' => $model,
            ]);
            }else{
                if (Yii::$app->user->identity->role == 11) {
                    $model = new SolicitudCrearAsociacion();
                    $model->id_estado_sol = 1;
                    $model->id_asocia = $id;
                    $model->fecha_envio = date('Y-m-d');
                    $model->id = Yii::$app->user->identity->id;
                    if ($model->load(Yii::$app->request->post()) && $model->save()) {
                        return $this->redirect(['view', 'id' => $model->id_solicitud_ca]);
                    }

                    return $this->render('create', [
                        'model' => $model,
                    ]);
                 }else{
                    return $this->render('../site/notpermis');
                }
            }
    }

    /**
     * Updates an existing SolicitudCrearAsociacion model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        if (Yii::$app->user->isGuest) {
            if (!Yii::$app->user->isGuest) {
                return $this->goHome();
            }
            $model = new LoginForm();
            if ($model->load(Yii::$app->request->post()) && $model->login()) {
                return $this->goBack();
            }
            $model->password = '';
            return $this->render('../site/login', [
                'model' => $model,
            ]);
            }else{
                if (Yii::$app->user->identity->role == 12) {
                    $model = $this->findModel($id);

                    if ($model->load(Yii::$app->request->post()) && $model->save()) {
                        return $this->redirect(['view', 'id' => $model->id_solicitud_ca]);
                    }
                    return $this->render('update_fun', [
                        'model' => $model,
                    ]);
                }
                if (Yii::$app->user->identity->role == 11) {
                    $model = $this->findModel($id);
                    if (Yii::$app->user->identity->id == $model->id) {

                    if ($model->load(Yii::$app->request->post()) && $model->save()) {
                        return $this->redirect(['view', 'id' => $model->id_solicitud_ca]);
                    }

                    return $this->render('update', [
                        'model' => $model,
                    ]);
                }else{
                    return $this->render('../site/notpermis');
                }
                }else{
                    return $this->render('../site/notpermis');
                }
            }
    }

    /**
     * Deletes an existing SolicitudCrearAsociacion model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        if(!Yii::$app->user->isGuest) {
            if (Yii::$app->user->identity->role == 12 || Yii::$app->user->identity->role == 10 || Yii::$app->user->identity->role == 11) {
            $this->findModel($id)->delete();

            return $this->redirect(['index']);
            }else{
                return $this->render('../site/notpermis');
            }
        }else{
            return $this->goHome();
        }
    }

    /**
     * Finds the SolicitudCrearAsociacion model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SolicitudCrearAsociacion the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SolicitudCrearAsociacion::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('Error, página no encontrada!');
    }
}
