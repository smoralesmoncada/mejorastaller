<?php

namespace app\controllers;

use Yii;
use app\models\Transgresion;
use app\models\LoginForm;
use app\models\TransgresionSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;

/**
 * TransgresionController implements the CRUD actions for Transgresion model.
 */
class TransgresionController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Transgresion models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (Yii::$app->user->isGuest) {
            if (!Yii::$app->user->isGuest) {
                return $this->goHome();
            }
            $model = new LoginForm();
            if ($model->load(Yii::$app->request->post()) && $model->login()) {
                return $this->goBack();
            }
            $model->password = '';
            return $this->render('../site/login', [
                'model' => $model,
            ]);
            }else{
                if (Yii::$app->user->identity->role == 12 ) {
                    $searchModel = new TransgresionSearch();
                    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

                    return $this->render('index_fun', [
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider,
                    ]);
                }
                if (Yii::$app->user->identity->role == 11 || Yii::$app->user->identity->role == 10) {
                $usuario = Yii::$app->user->identity->id;
                $query = Transgresion::find()->where(['id' => $usuario]);
                $provider = new ActiveDataProvider([
                    'query' => $query,
                    'pagination' => [
                        'pageSize' => 10,
                    ],
                    
                ]);

                return $this->render('index', [
                    //'searchModel' => $searchModel,
                    'dataProvider' => $provider,
                ]);
            }else{
                    return $this->render('../site/notpermis');
                }
                
            }
    }

    /**
     * Displays a single Transgresion model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        if (Yii::$app->user->isGuest) {
            if (!Yii::$app->user->isGuest) {
                return $this->goHome();
            }
            $model = new LoginForm();
            if ($model->load(Yii::$app->request->post()) && $model->login()) {
                return $this->goBack();
            }
            $model->password = '';
            return $this->render('../site/login', [
                'model' => $model,
            ]);
            }else{
                if (Yii::$app->user->identity->role == 11 || Yii::$app->user->identity->role == 12 || Yii::$app->user->identity->role == 10) {
                return $this->render('view', [
                    'model' => $this->findModel($id),
                ]);
            }else{
                    return $this->render('../site/notpermis');
                }
                
            }   
    }

    /**
     * Creates a new Transgresion model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (Yii::$app->user->isGuest) {
            if (!Yii::$app->user->isGuest) {
                return $this->goHome();
            }
            $model = new LoginForm();
            if ($model->load(Yii::$app->request->post()) && $model->login()) {
                return $this->goBack();
            }
            $model->password = '';
            return $this->render('../site/login', [
                'model' => $model,
            ]);
            }else{
                if (Yii::$app->user->identity->role == 11 || Yii::$app->user->identity->role == 12 || Yii::$app->user->identity->role == 10) {
                $model = new Transgresion();
                $model->id = Yii::$app->user->identity->id;
                //$time = date_default_timezone_set("America/Santiago");
                $model->fecha_denuncia = date("d-m-Y");
                $model->fecha_modificacion = date("d-m-Y");
                    if ($model->load(Yii::$app->request->post()) && $model->save()) {
                        return $this->redirect(['view', 'id' => $model->id_transgresion]);
                    }

                    return $this->render('create', [
                        'model' => $model,
                        ]);
            }else{
                    return $this->render('../site/notpermis');
                }
                
            } 
    }

    /**
     * Updates an existing Transgresion model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        if (Yii::$app->user->isGuest) {
            if (!Yii::$app->user->isGuest) {
                return $this->goHome();
            }
            $model = new LoginForm();
            if ($model->load(Yii::$app->request->post()) && $model->login()) {
                return $this->goBack();
            }
            $model->password = '';
            return $this->render('../site/login', [
                'model' => $model,
            ]);
            }else{
                if (Yii::$app->user->identity->role == 11 || Yii::$app->user->identity->role == 12 || Yii::$app->user->identity->role == 10 ) {
            $model = $this->findModel($id);
            $model->fecha_modificacion = date("d-m-Y");
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id_transgresion]);
            }

            return $this->render('update', [
                'model' => $model,
            ]);
            }else{
                    return $this->render('../site/notpermis');
                }
                
            } 
    }

    /**
     * Deletes an existing Transgresion model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        if (Yii::$app->user->isGuest) {
            if (!Yii::$app->user->isGuest) {
                return $this->goHome();
            }
            $model = new LoginForm();
            if ($model->load(Yii::$app->request->post()) && $model->login()) {
                return $this->goBack();
            }
            $model->password = '';
            return $this->render('../site/login', [
                'model' => $model,
            ]);
            }else{
                if (Yii::$app->user->identity->role == 11 || Yii::$app->user->identity->role == 12 || Yii::$app->user->identity->role == 10) {
            $this->findModel($id)->delete();

            return $this->redirect(['index']);
            }else{
                    return $this->render('../site/notpermis');
                }
                
            }
    }

    /**
     * Finds the Transgresion model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Transgresion the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Transgresion::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('Error, página no encontrada!');
    }
}
