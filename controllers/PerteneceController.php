<?php

namespace app\controllers;

use Yii;
use app\models\Pertenece;
use app\models\LoginForm;
use app\models\PerteneceSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;

/**
 * PerteneceController implements the CRUD actions for Pertenece model.
 */
class PerteneceController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Pertenece models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (Yii::$app->user->isGuest) {
            if (!Yii::$app->user->isGuest) {
                return $this->goHome();
            }
            $model = new LoginForm();
            if ($model->load(Yii::$app->request->post()) && $model->login()) {
                return $this->goBack();
            }
            $model->password = '';
            return $this->render('../site/login', [
                'model' => $model,
            ]);
            }else{
                if (Yii::$app->user->identity->role == 11 && Yii::$app->user->identity->directiva != true) {
                    $usuario = Yii::$app->user->identity->id;
                    $query = Pertenece::find()->where(['id' => $usuario]);
                    $provider = new ActiveDataProvider([
                        'query' => $query,
                        'pagination' => [
                            'pageSize' => 10,
                        ],
                        
                    ]);

                    return $this->render('index_est', [
                        //'searchModel' => $searchModel,
                        'dataProvider' => $provider,
                    ]);
                }
                if (Yii::$app->user->identity->directiva == true) {
                    $searchModel = new PerteneceSearch();
                    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

                    return $this->render('index', [
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider,
                    ]);

                }
                if (Yii::$app->user->identity->role == 12) {
                    $searchModel = new PerteneceSearch();
                    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

                    return $this->render('index', [
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider,
                    ]);
                }else{
                    return $this->render('../site/notpermis');
                }
                
            }
    }

    /**
     * Displays a single Pertenece model.
     * @param integer $id_situacion
     * @param integer $rut_alumno
     * @param integer $id_asocia
     * @param integer $id_cargo
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id_situacion, $rut_alumno, $id_asocia, $id_cargo)
    {
        if (Yii::$app->user->isGuest) {
            if (!Yii::$app->user->isGuest) {
                return $this->goHome();
            }
            $model = new LoginForm();
            if ($model->load(Yii::$app->request->post()) && $model->login()) {
                return $this->goBack();
            }
            $model->password = '';
            return $this->render('../site/login', [
                'model' => $model,
            ]);
            }else{
                if (Yii::$app->user->identity->role == 12) {
                    return $this->render('view', [
                'model' => $this->findModel($id_situacion, $rut_alumno, $id_asocia, $id_cargo),
            ]);
            }
                if (Yii::$app->user->identity->role == 11 && Yii::$app->user->identity->directiva == true) {
                    return $this->render('view', [
                'model' => $this->findModel($id_situacion, $rut_alumno, $id_asocia, $id_cargo),
            ]);
            }else{
                    return $this->render('../site/notpermis');
                }
                
            }
    }

    /**
     * Creates a new Pertenece model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (Yii::$app->user->isGuest) {
            if (!Yii::$app->user->isGuest) {
                return $this->goHome();
            }
            $model = new LoginForm();
            if ($model->load(Yii::$app->request->post()) && $model->login()) {
                return $this->goBack();
            }
            $model->password = '';
            return $this->render('../site/login', [
                'model' => $model,
            ]);
            }else{
                if (Yii::$app->user->identity->role == 11 && Yii::$app->user->identity->directiva == true) {
                    $model = new Pertenece();
                    $model->id_cargo = 4;
                    $model->id_situacion = 1;            

                    $conection=Yii::$app->db;

                    if ($model->load(Yii::$app->request->post()) && $model->save()) {
                        $usuario = (new \yii\db\Query())
                                    ->select('id')
                                    ->from('alumno')
                                    ->where('rut_alumno='.$model->rut_alumno);
                        $rows = $usuario->all();
                        $command = $usuario->createCommand();
                        $row = $command->queryAll();

                        //$conection->createCommand("UPDATE pertenece SET id=:".$usuario."WHERE rut_alumno=:".$model->rut_alumno)->execute();

                        /*$update = (new \yii\db\Command())
                                    ->update('pertenece')
                                    ->set('id='.$usuario)
                                    ->where('rut_alumno='.$model->rut_alumno);
                        $command = $usuario->createCommand();
                        $usuario = $command->execute();*/

                        $conection->createCommand()->update('pertenece', ['id'=>$usuario], 'rut_alumno='.$model->rut_alumno)->execute();

                        return $this->redirect(['view', 'id_situacion' => $model->id_situacion, 'rut_alumno' => $model->rut_alumno, 'id_asocia' => $model->id_asocia, 'id_cargo' => $model->id_cargo]);
                    }

                    return $this->render('create', [
                        'model' => $model,
                    ]);
                    }

                    if (Yii::$app->user->identity->role == 12) {
                    $model = new Pertenece();
                    $model->id_cargo = 4;
                    $model->id_situacion = 1;
                     $conection=Yii::$app->db;

                    if ($model->load(Yii::$app->request->post()) && $model->save()) {
                        $usuario = (new \yii\db\Query())
                                    ->select('id')
                                    ->from('alumno')
                                    ->where('rut_alumno='.$model->rut_alumno);
                        $rows = $usuario->all();
                        $command = $usuario->createCommand();
                        $row = $command->queryAll();
                        
                        if ($model->id_cargo == 1 || $model->id_cargo == 2 || $model->id_cargo == 3) {
                             $conection->createCommand()->update('pertenece', ['directiva'=>true], 'rut_alumno='.$model->rut_alumno)->execute();
                        }

                        //$conection->createCommand("UPDATE pertenece SET id=:".$usuario."WHERE rut_alumno=:".$model->rut_alumno)->execute();

                        /*$update = (new \yii\db\Command())
                                    ->update('pertenece')
                                    ->set('id='.$usuario)
                                    ->where('rut_alumno='.$model->rut_alumno);
                        $command = $usuario->createCommand();
                        $usuario = $command->execute();*/

                        $conection->createCommand()->update('pertenece', ['id'=>$usuario], 'rut_alumno='.$model->rut_alumno)->execute();

                        return $this->redirect(['view', 'id_situacion' => $model->id_situacion, 'rut_alumno' => $model->rut_alumno, 'id_asocia' => $model->id_asocia, 'id_cargo' => $model->id_cargo]);
                    }

                    return $this->render('create', [
                        'model' => $model,
                    ]);
                    }else{
                    return $this->render('../site/notpermis');
                }
                
            }
    }

    /**
     * Updates an existing Pertenece model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id_situacion
     * @param integer $rut_alumno
     * @param integer $id_asocia
     * @param integer $id_cargo
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id_situacion, $rut_alumno, $id_asocia, $id_cargo)
    {
        if (Yii::$app->user->isGuest) {
            if (!Yii::$app->user->isGuest) {
                return $this->goHome();
            }
            $model = new LoginForm();
            if ($model->load(Yii::$app->request->post()) && $model->login()) {
                return $this->goBack();
            }
            $model->password = '';
            return $this->render('../site/login', [
                'model' => $model,
            ]);
            }else{
                if (Yii::$app->user->identity->role == 11 && Yii::$app->user->identity->directiva == true) {
                    $model = $this->findModel($id_situacion, $rut_alumno, $id_asocia, $id_cargo);

                    if ($model->load(Yii::$app->request->post()) && $model->save()) {
                        return $this->redirect(['view', 'id_situacion' => $model->id_situacion, 'rut_alumno' => $model->rut_alumno, 'id_asocia' => $model->id_asocia, 'id_cargo' => $model->id_cargo]);
                    }

                    return $this->render('update', [
                        'model' => $model,
                    ]);
                    }
                    if (Yii::$app->user->identity->role == 12) {
                    $model = $this->findModel($id_situacion, $rut_alumno, $id_asocia, $id_cargo);

                    if ($model->load(Yii::$app->request->post()) && $model->save()) {
                        return $this->redirect(['view', 'id_situacion' => $model->id_situacion, 'rut_alumno' => $model->rut_alumno, 'id_asocia' => $model->id_asocia, 'id_cargo' => $model->id_cargo]);
                    }

                    return $this->render('update', [
                        'model' => $model,
                    ]);
                    }else{
                    return $this->render('../site/notpermis');
                }
                
            }
    }

    /**
     * Deletes an existing Pertenece model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id_situacion
     * @param integer $rut_alumno
     * @param integer $id_asocia
     * @param integer $id_cargo
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id_situacion, $rut_alumno, $id_asocia, $id_cargo)
    {
        if (Yii::$app->user->isGuest) {
            if (!Yii::$app->user->isGuest) {
                return $this->goHome();
            }
            $model = new LoginForm();
            if ($model->load(Yii::$app->request->post()) && $model->login()) {
                return $this->goBack();
            }
            $model->password = '';
            return $this->render('../site/login', [
                'model' => $model,
            ]);
            }else{
                if (Yii::$app->user->identity->role == 12) {
                    $this->findModel($id_situacion, $rut_alumno, $id_asocia, $id_cargo)->delete();

                    return $this->redirect(['index']);
                }
                if (Yii::$app->user->identity->role == 11 && Yii::$app->user->identity->directiva == true) {
                    $this->findModel($id_situacion, $rut_alumno, $id_asocia, $id_cargo)->delete();

                    return $this->redirect(['index']);
                }else{
                    return $this->render('../site/notpermis');
                }

            }
    }

    /**
     * Finds the Pertenece model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id_situacion
     * @param integer $rut_alumno
     * @param integer $id_asocia
     * @param integer $id_cargo
     * @return Pertenece the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id_situacion, $rut_alumno, $id_asocia, $id_cargo)
    {
        if (($model = Pertenece::findOne(['id_situacion' => $id_situacion, 'rut_alumno' => $rut_alumno, 'id_asocia' => $id_asocia, 'id_cargo' => $id_cargo])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('Error, página no encontrada!');
    }
}
