<?php

namespace app\controllers;

use Yii;
use app\models\Funcionario;
use app\models\LoginForm;
use app\models\FuncionarioSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * FuncionarioController implements the CRUD actions for Funcionario model.
 */
class FuncionarioController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Funcionario models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (Yii::$app->user->isGuest) {
            if (!Yii::$app->user->isGuest) {
                return $this->goHome();
            }
            $model = new LoginForm();
            if ($model->load(Yii::$app->request->post()) && $model->login()) {
                return $this->goBack();
            }
            $model->password = '';
            return $this->render('../site/login', [
                'model' => $model,
            ]);
            }else{
                if (Yii::$app->user->identity->role == 10) {
                    $searchModel = new FuncionarioSearch();
                    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
                    return $this->render('index', [
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider,
                    ]);
                }else{
                    return $this->render('../site/notpermis');
                }
                
            }
    }

    /**
     * Displays a single Funcionario model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        if (Yii::$app->user->isGuest) {
            if (!Yii::$app->user->isGuest) {
                return $this->goHome();
            }
            $model = new LoginForm();
            if ($model->load(Yii::$app->request->post()) && $model->login()) {
                return $this->goBack();
            }
            $model->password = '';
            return $this->render('../site/login', [
                'model' => $model,
            ]);
            }else{
                if (Yii::$app->user->identity->role == 10) {
                    return $this->render('view', [
                        'model' => $this->findModel($id),
                    ]);
                }else{
                    return $this->render('../site/notpermis');
                }
            }
    }

    /**
     * Creates a new Funcionario model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
                if (Yii::$app->user->isGuest) {
            if (!Yii::$app->user->isGuest) {
                return $this->goHome();
            }
            $model = new LoginForm();
            if ($model->load(Yii::$app->request->post()) && $model->login()) {
                return $this->goBack();
            }
            $model->password = '';
            return $this->render('../site/login', [
                'model' => $model,
            ]);
            }else{
                if (Yii::$app->user->identity->role == 10) {
                    $model = new Funcionario();
                    if ($model->load(Yii::$app->request->post()) && $model->save()) {
                        return $this->redirect(['view', 'id' => $model->rut_fun]);
                    }
                    return $this->render('create', [
                        'model' => $model,
                    ]);
                }else{
                    return $this->render('../site/notpermis');
                }
            }
    }

    /**
     * Updates an existing Funcionario model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        if (Yii::$app->user->isGuest) {
            if (!Yii::$app->user->isGuest) {
                return $this->goHome();
            }
            $model = new LoginForm();
            if ($model->load(Yii::$app->request->post()) && $model->login()) {
                return $this->goBack();
            }
            $model->password = '';
            return $this->render('../site/login', [
                'model' => $model,
            ]);
            }else{
                if (Yii::$app->user->identity->role == 10) {
                    $model = $this->findModel($id);

                    if ($model->load(Yii::$app->request->post()) && $model->save()) {
                        return $this->redirect(['view', 'id' => $model->rut_fun]);
                    }

                    return $this->render('update', [
                        'model' => $model,
                    ]);
                }else{
                    return $this->render('../site/notpermis');
                }
            }
    }

    /**
     * Deletes an existing Funcionario model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        if (Yii::$app->user->isGuest) {
            if (!Yii::$app->user->isGuest) {
                return $this->goHome();
            }
            $model = new LoginForm();
            if ($model->load(Yii::$app->request->post()) && $model->login()) {
                return $this->goBack();
            }
            $model->password = '';
            return $this->render('../site/login', [
                'model' => $model,
            ]);
            }else{
                if (Yii::$app->user->identity->role == 10) {
                    $this->findModel($id)->delete();

                    return $this->redirect(['index']);
                }else{
                        return $this->render('../site/notpermis');
                }
            }
    }

    /**
     * Finds the Funcionario model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Funcionario the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Funcionario::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('Error, página no encontrada!');
    }
}
