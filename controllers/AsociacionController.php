<?php

namespace app\controllers;

use Yii;
use app\models\Asociacion;
use app\models\LoginForm;
use app\models\AsociacionSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;


/**
 * AsociacionController implements the CRUD actions for Asociacion model.
 */
class AsociacionController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Asociacion models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (Yii::$app->user->isGuest) {
            if (!Yii::$app->user->isGuest) {
                return $this->goHome();
            } 
            $model = new LoginForm();
            if ($model->load(Yii::$app->request->post()) && $model->login()) {
                return $this->goBack();
            }
            $model->password = '';
            return $this->render('../site/login', [
                'model' => $model,
            ]);
            }else{
                if (Yii::$app->user->identity->role == 11 || Yii::$app->user->identity->role == 10) {
        
                    $usuario = Yii::$app->user->identity->id;
                $query = Asociacion::find()->where(['id' => $usuario]);
                $provider = new ActiveDataProvider([
                    'query' => $query,
                    'pagination' => [
                        'pageSize' => 10,
                    ],
                    
                ]);

                return $this->render('index', [
                    //'searchModel' => $searchModel,
                    'dataProvider' => $provider,
                ]);
                }
                if (Yii::$app->user->identity->role == 12) {
                    $searchModel = new AsociacionSearch();
                    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

                    return $this->render('index_fun', [
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider,
                    ]);

                }else{
                    return $this->render('../site/notpermis');
                }
                
            }
        
    }

    /**
     * Displays a single Asociacion model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        if (Yii::$app->user->isGuest) {
            if (!Yii::$app->user->isGuest) {
                return $this->goHome();
            }
            $model = new LoginForm();
            if ($model->load(Yii::$app->request->post()) && $model->login()) {
                return $this->goBack();
            }
            $model->password = '';
            return $this->render('../site/login', [
                'model' => $model,
            ]);
            }else{
                if (Yii::$app->user->identity->role == 12) {
                    return $this->render('view', [
                        'model' => $this->findModel($id),
                    ]);
                }
                if (Yii::$app->user->identity->role == 11 || Yii::$app->user->identity->role == 10) {
                    $asociacion = $this->findModel($id);      
                    if (Yii::$app->user->identity->id == $asociacion->id) {
                        return $this->render('view', [
                        'model' => $this->findModel($id),
                    ]);
                    }else{
                    return $this->render('../site/notpermis');
                    } 
                }else{
                    return $this->render('../site/notpermis');
                }
            }
    }

    /**
     * Creates a new Asociacion model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (Yii::$app->user->isGuest) {
            if (!Yii::$app->user->isGuest) {
                return $this->goHome();
            }
            $model = new LoginForm();
            if ($model->load(Yii::$app->request->post()) && $model->login()) {
                return $this->goBack();
            }
            $model->password = '';
            return $this->render('../site/login', [
                'model' => $model,
            ]);
            }else{
                if (Yii::$app->user->identity->role == 11 || Yii::$app->user->identity->role == 10) {  
                    $model = new Asociacion(); 
                    $model->id_estado_asocia = 3;
                    //$model->fecha_inicio_asocia = date('Y-m-d');
                    $model->id = Yii::$app->user->identity->id;
                    if ($model->load(Yii::$app->request->post()) && $model->save()) {
                        return $this->redirect(['view','id' => $model->id_asocia]);
                    }

                    return $this->render('create', [
                        'model' => $model,
                    ]);
                }else{
                    return $this->render('../site/notpermis');
                }
            }  
    }
    /**
     * Updates an existing Asociacion model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        if (Yii::$app->user->isGuest) {
            if (!Yii::$app->user->isGuest) {
                return $this->goHome();
            }
            $model = new LoginForm();
            if ($model->load(Yii::$app->request->post()) && $model->login()) {
                return $this->goBack();
            }
            $model->password = '';
            return $this->render('../site/login', [
                'model' => $model,
            ]);
            }else{
                if (Yii::$app->user->identity->role == 12) {
                    $model = $this->findModel($id);

                    if ($model->load(Yii::$app->request->post()) && $model->save()) {
                        return $this->redirect(['view', 'id' => $model->id_asocia]);
                    }
                    return $this->render('update_fun', [
                        'model' => $model,
                    ]);
                }
                if (Yii::$app->user->identity->role == 11 || Yii::$app->user->identity->role == 10) { 
                    $model = $this->findModel($id);     
                    if (Yii::$app->user->identity->id == $model->id) {
                    if ($model->load(Yii::$app->request->post()) && $model->save()) {
                        return $this->redirect(['view', 'id' => $model->id_asocia]);
                    }

                    return $this->render('update', [
                        'model' => $model,
                    ]);
                }else{
                    return $this->render('../site/notpermis');
                }
                }else{
                    return $this->render('../site/notpermis');
                }
            }     
    }

    /**
     * Deletes an existing Asociacion model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        if (Yii::$app->user->isGuest) {
            if (!Yii::$app->user->isGuest) {
                return $this->goHome();
            }
            $model = new LoginForm();
            if ($model->load(Yii::$app->request->post()) && $model->login()) {
                return $this->goBack();
            }
            $model->password = '';
            return $this->render('../site/login', [
                'model' => $model,
            ]);
            }else{
                if (Yii::$app->user->identity->role == 12 || Yii::$app->user->identity->role == 10) { 
                    $model = $this->findModel($id);
                   if (Yii::$app->user->identity->id == $model->id) {
                    $this->findModel($id)->delete();
                    return $this->redirect(['index']);
                    }else{
                    return $this->render('../site/notpermis');
                }
                }else{
                    return $this->render('../site/notpermis');
                }
            } 

    }

    /**
     * Finds the Asociacion model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Asociacion the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Asociacion::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('Error, página no encontrada!');
    }
}
